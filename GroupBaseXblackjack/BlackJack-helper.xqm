xquery version "3.0";

module namespace BlackJack-helper = "BlackJack/Helper";
import module namespace BlackJack-main = "BlackJack/Main" at "BlackJack-main.xqm";

declare function BlackJack-helper:getFirstPlayer($id){
  let $firstPlayer := BlackJack-main:getGame($id)/Characters/Player[@online = 1][1]
  return (
    $firstPlayer
  )
};

declare function BlackJack-helper:getCurrentPlayer($id){
  let $playerID := BlackJack-main:getGame($id)/PlayerTurn
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  return (
    $player
  )
};

declare function BlackJack-helper:getNextPlayer($id, $playerID){
  let $nextPlayer := 
  if ($playerID = 8) then BlackJack-helper:getFirstPlayer($id)
  else BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]/following-sibling::Player[@online = 1][1]
  return (
    $nextPlayer
  )
};

declare function BlackJack-helper:getLastPlayer($id){
  let $lastPlayer := BlackJack-main:getGame($id)/Characters/Player[@online = 1][last()-1]
  return (
    $lastPlayer
  )
};

declare function BlackJack-helper:hasUnactiveAss($id, $playerID){
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  let $ass := $player/Cards/Card[Value = 11]
  return ( 
    if (count($ass) > 0) then true() else false()
  )
};

declare 
function BlackJack-helper:checkScore($id, $playerID){
  let $score := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]/Score
  let $player := BlackJack-helper:getCurrentPlayer($id)
  return (
    if ($playerID = xs:integer(8)) then (
      if ($score < 17) then true()
      else false() )
    else (
      if ($score < 21) then true()
      else false() )
  )
};

declare function BlackJack-helper:randCard($id){
  let $deckSize := count(BlackJack-main:getGame($id)/Deck/Card)
  let $randNbr := random:integer($deckSize)+1
  let $newCard := BlackJack-main:getGame($id)/Deck/Card[$randNbr]
  return (
    $newCard
  )
};

declare
function BlackJack-helper:newDeck(){
  let $newDeck := 
  <Deck>
    {for $nbrOfDecks in (1 to 3) return (
      for $sign in doc("../static/GroupBaseXblackjack/Cards/Deck.xml")/Deck/Signs/Sign return (
        for $nbr in doc("../static/GroupBaseXblackjack/Cards/Deck.xml")/Deck/Numbers/Nbr return (
      <Card>
        {$nbr}
        {$sign}
        <Value>
        {if ($nbr = "J" or $nbr = "Q" or $nbr = "K") then 10
        else if ($nbr = "A") then 11
        else number($nbr)}
        </Value>
      </Card>
      )
    )
  )
}
</Deck>
  return (
    $newDeck
  )
};