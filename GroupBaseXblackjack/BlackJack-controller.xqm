xquery version "3.0";

module namespace BlackJack-controller = "BlackJack-controller.xqm";
import module namespace BlackJack-main = "BlackJack/Main" at "BlackJack-main.xqm";
import module namespace BlackJack-helper = "BlackJack/Helper" at "BlackJack-helper.xqm";

declare variable $BlackJack-controller:staticPath := "../static/GroupBaseXblackjack";

declare
%rest:path("GroupBaseX/setup")
%output:method("xhtml")
%updating
%rest:GET
function BlackJack-controller:setup(){
  let $bjConfig := doc("../static/GroupBaseXblackjack/GroupBaseXBlackJackConfiguration.xml")
  let $redirectLink := "/GroupBaseX/LobbyStart"
  return(
    db:create("GroupBaseX",$bjConfig),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("GroupBaseX/{$id}/startGame")
%output:method("html")
%updating
%rest:GET
function BlackJack-controller:startGame($id){
  let $redirectLink := concat("/GroupBaseX/",$id,"/draw")
  return (
    BlackJack-main:setFirstPlayer($id),
    BlackJack-main:updatePlayerNbr($id),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("GroupBaseX/{$id}/addPlayer/{$playerID}")
%output:method("html")
%updating
%rest:GET
function BlackJack-controller:addPlayer($id, $playerID){
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  return (
    replace value of node $player/@online with 1,
    replace value of node  $player/Stock with 300,
    update:output(web:redirect(concat("/GroupBaseX/",$id,"/choosePlayer")))
  )
};

declare
%rest:path("GroupBaseX/{$id}/removePlayer/{$playerID}")
%output:method("html")
%updating
%rest:GET
function BlackJack-controller:removePlayer($id, $playerID){
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  return (
    replace value of node $player/@online with 0,
    update:output(web:redirect(concat("/GroupBaseX/",$id,"/choosePlayer")))
  )
};

declare
%rest:path("GroupBaseX/{$id}/setPlayer")
%output:method("html")
%updating
%rest:GET
function BlackJack-controller:setPlayer($id){
  let $playerName := BlackJack-main:getGame($id)/Characters/Player[@id = 3]/@name
  return (
    for $player in BlackJack-main:getGame($id)/Characters/Player return (
      let $playerName := concat("PlayerName", $player/@id)
      let $req := concat("",request:parameter($playerName))
      return replace value of node $player/@name with $req
    ),
    update:output(web:redirect(concat("/GroupBaseX/",$id,"/startGame")))
  )
};

declare
%rest:path("GroupBaseX/{$id}/addChip/{$value}")
%output:method("html")
%updating
%rest:GET
function BlackJack-controller:addChip($id, $value){
  let $redirectLink := concat("/GroupBaseX/",$id,"/draw")
  return (
    BlackJack-main:processAddChip($id, $value),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/undo")
%rest:GET
%updating
function BlackJack-controller:undo($id){
  let $redirectLink := concat("/GroupBaseX/",$id,"/draw")
  return (
    BlackJack-main:processUndo($id),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/bet")
%rest:GET
%updating
function BlackJack-controller:bet($id){
  let $playerID := BlackJack-helper:getCurrentPlayer($id)/@id
  let $nextPlayer := BlackJack-helper:getNextPlayer($id, $playerID)
  return (
    BlackJack-main:updatePlayerTurn($id, $nextPlayer/@id),
    if ($nextPlayer/@id = 8) then (
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/newDeck")))
    ) else
    update:output(web:redirect(concat("/GroupBaseX/",$id,"/draw")))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/allin/{$stock}")
%rest:GET
%updating
function BlackJack-controller:processAllIn($id, $stock as xs:integer){
  let $player := BlackJack-helper:getCurrentPlayer($id)
  let $chip := 
   if ($stock >= 500) then 500
   else if ($stock >= 100) then 100
   else if ($stock >= 25) then 25
   else if ($stock >= 10) then 10
   else if ($stock >= 5) then 5
   else if ($stock >= 1) then 1
   else 0
  let $newStock := $stock - $chip
  return (
    replace value of node $player/CurrentBet with $player/CurrentBet + $chip,
    replace value of node $player/Stock with $newStock,
    if ($chip != 0) then (
      insert node <Chip><Nbr>{$chip}</Nbr></Chip> into $player/Chips,
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/allin/", $newStock))) )
    else update:output(web:redirect(concat("/GroupBaseX/",$id,"/draw")))
  )
};

declare
%rest:GET
%updating
%output:method("html")
%rest:path("GroupBaseX/{$id}/newDeck")
function BlackJack-controller:newDeck($id){
  let $redirectLink := concat("/GroupBaseX/",$id,"/newCardsRec/", BlackJack-helper:getFirstPlayer($id)/@id)
  return (
    BlackJack-main:newDeck($id),
    BlackJack-main:setFirstPlayer($id),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:GET
%updating
%output:method("html")
%rest:path("GroupBaseX/{$id}/newCardsRec/{$playerID}")
function BlackJack-controller:newCardsRec($id, $playerID){
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  let $lastPlayerCards := BlackJack-helper:getLastPlayer($id)/Cards/Card
  let $nextPlayer := BlackJack-helper:getNextPlayer($id, $playerID)
  return (
    if (count($lastPlayerCards) = 2) then update:output(web:redirect(concat("/GroupBaseX/",$id,"/checkAss")))
    else ( update:output(web:redirect(concat("/GroupBaseX/",$id,"/newCardsRec/", $nextPlayer/@id))), BlackJack-main:drawCard($id, $player/@id))
  )
};

declare
%rest:GET
%updating
%output:method("html")
%rest:path("GroupBaseX/{$id}/checkAss")
function BlackJack-controller:checkAss($id){
  let $firstPlayer := BlackJack-helper:getFirstPlayer($id)
  let $playersOnline := BlackJack-main:getGame($id)/Characters/Player[@online = 1]
  return (
    BlackJack-main:checkAss($id),
    BlackJack-main:updateGamePhase($id),
    if ($firstPlayer/Score = 21) then 
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/stand/", $firstPlayer/@id)))
    else
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/draw")))
  )
  
};

declare
%rest:path("/GroupBaseX/{$id}/hit/{$playerID}")
%rest:GET
%updating
function BlackJack-controller:processHit($id, $playerID as xs:integer){
  let $redirectLink := concat ("/GroupBaseX/",$id,"/checkScore/", $playerID)
  return (
    BlackJack-main:drawCard($id, $playerID), 
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/checkScore/{$playerID}")
%rest:GET
%updating
function BlackJack-controller:checkScore($id, $playerID as xs:integer){
  let $redirectLink :=  concat ("/GroupBaseX/",$id,"/stand/", $playerID)
  let $continue := BlackJack-helper:checkScore($id, $playerID)
  let $player := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  return (
    if ($continue) then update:output(web:redirect(concat("/GroupBaseX/",$id,"/draw")))
    else if (BlackJack-helper:hasUnactiveAss($id, $playerID) and $player/Score -10 < 21) then (
      replace value of node $player/Cards/Card[Value = 11][1]/Value with 1,
      BlackJack-main:updateScore($id, $playerID , -10),
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/draw")))
    ) else if ($player/Score -10 = 21) then (
      BlackJack-main:updateScore($id, $playerID , -10),
      update:output(web:redirect($redirectLink))
    ) else update:output(web:redirect($redirectLink))
)
};

declare
%rest:path("/GroupBaseX/{$id}/stand/{$playerID}")
%rest:GET
%updating
function BlackJack-controller:processStand($id, $playerID as xs:integer){
  let $nextPlayerID := BlackJack-helper:getNextPlayer($id, $playerID)/@id
  let $nextPlayerScore := BlackJack-main:getGame($id)/Characters/Player[@id = $nextPlayerID]/Score
  let $nextPlayerToPlayID := 
  if ($nextPlayerScore = 21) then 
    BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]/following-sibling::Player[@online = 1 and Score != 21][1]/@id
  else $nextPlayerID
  let $redirectLink := 
  if ($nextPlayerToPlayID = 8) then concat("/GroupBaseX/",$id,"/dealer")
  else concat("/GroupBaseX/",$id,"/draw")
  return (
    BlackJack-main:updatePlayerTurn($id, $nextPlayerToPlayID),
    update:output(web:redirect($redirectLink)))
};

declare
%rest:path("/GroupBaseX/{$id}/dealer")
%rest:GET
%updating
function BlackJack-controller:dealer($id){
  let $dealer := BlackJack-main:getGame($id)/Characters/Player[@id = 8]
  let $continue := BlackJack-helper:checkScore($id, 8)
  return (
    if ($continue) then (
      BlackJack-main:drawCard($id, $dealer/@id), 
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/dealer")))
    ) else if (BlackJack-helper:hasUnactiveAss($id, 8) and $dealer/Score > 21) then (
      replace value of node $dealer/Cards/Card[Value = 11][1]/Value with 1,
      BlackJack-main:updateScore($id, 8 , -10),
      update:output(web:redirect(concat("/GroupBaseX/",$id,"/dealer")))
    ) else update:output(web:redirect(concat("/GroupBaseX/",$id,"/endGame")))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/endGame")
%rest:GET
%updating
function BlackJack-controller:endGame($id){
  let $dealerScore := BlackJack-main:getGame($id)/Characters/Player[@id = 8]/Score
  return (
  for $player in BlackJack-main:getGame($id)/Characters/Player[@online = 1]
    return (
      if ($player/@id = 8) then (
        BlackJack-main:updateGamePhase($id),
        update:output(web:redirect(concat("/GroupBaseX/",$id,"/highscore"))))
      else if ( ($player/Score < 22 and ($dealerScore < $player/Score or $dealerScore > 21))) then 
        BlackJack-main:updateStock($id, $player, 2*$player/CurrentBet)
      else if ($player/Score < 22 and $dealerScore = $player/Score) then 
        BlackJack-main:updateStock($id, $player, $player/CurrentBet)
    )
  ) 
};

declare
%updating
%rest:path("/GroupBaseX/{$id}/highscore")
%rest:GET
function BlackJack-controller:updateHighscore($id){
  let $playersOnline := BlackJack-main:getGame($id)/Characters/Player[@online = 1]
  return (
    for $player in $playersOnline return (
      if ($player/@id = 8) then update:output(web:redirect(concat("/GroupBaseX/",$id,"/highscoreSort")))
      else (
        BlackJack-main:updateHighscore($id, $player)
      )
    )
  )
};

declare
%updating
%rest:path("/GroupBaseX/{$id}/highscoreSort")
%rest:GET
function BlackJack-controller:updateHighscoreSort($id){
  let $redirectLink := concat("/GroupBaseX/",$id,"/draw")
  return (
    BlackJack-main:updateHighscoreSort(),
    update:output(web:redirect($redirectLink))
  )
};

declare
%rest:path("/GroupBaseX/{$id}/reset")
%rest:GET
%updating
function BlackJack-controller:newRound($id){
    let $redirectLink := concat("/GroupBaseX/",$id,"/startGame")
    return (
      BlackJack-main:resetRound($id),
      BlackJack-main:updateGamePhase($id),
      update:output(web:redirect($redirectLink))
    )
};

declare 
%rest:path("/GroupBaseX/{$id}/draw")
%output:method("html")
%rest:GET
function BlackJack-controller:drawGame($id){
  let $xslStylesheet := concat ("BlackJack/GroupBaseXBlackJackRealisation", $id, ".xslt")
  let $title := "GroupBaseX BlackJack"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare
%rest:path("/GroupBaseX/LobbyStart")
%output:method("html")
%rest:GET
function BlackJack-controller:drawLobby(){
  let $xslStylesheet := "Lobby/GroupBaseXBlackJackLobbyStart.xslt"
  let $title := "GroupBaseX BlackJack Lobby"
  let $game := BlackJack-main:getRootNode()
  return (
    BlackJack-controller:generatePage($game, $xslStylesheet, $title) 
  )
};

declare
%rest:path("/GroupBaseX/LobbyHelp")
%output:method("html")
%rest:GET
function BlackJack-controller:drawLobbyHelp(){
  let $xslStylesheet := "Lobby/GroupBaseXBlackJackLobbyHelp.xslt"
  let $title := "GroupBaseX BlackJack Lobby Help"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare
%rest:path("/GroupBaseX/ChooseGame")
%output:method("html")
%rest:GET
function BlackJack-controller:drawChooseGame(){
  let $xslStylesheet := "Lobby/GroupBaseXBlackJackLobbyChooseGame.xslt"
  let $title := "GroupBaseX BlackJack Lobby Choose Game"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare
%rest:path("/GroupBaseX/LobbyScore")
%output:method("html")
%rest:GET
function BlackJack-controller:drawLobbyScore(){
  let $xslStylesheet := "Lobby/GroupBaseXBlackJackLobbyScore.xslt"
  let $title := "GroupBaseX BlackJack Lobby Score"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare
%rest:path("/GroupBaseX/TutorialBet")
%output:method("html")
%rest:GET
function BlackJack-controller:drawTutorialBet(){
  let $xslStylesheet := "Tutorial/GroupBaseXBlackJackTutorialBet.xslt"
  let $title := "GroupBaseX BlackJack Tutorial Bet"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};
declare
%rest:path("/GroupBaseX/TutorialBetFinal")
%output:method("html")
%rest:GET
function BlackJack-controller:drawTutorialBetFinal(){
  let $xslStylesheet := "Tutorial/GroupBaseXBlackJackTutorialBetFinal.xslt"
  let $title := "GroupBaseX BlackJack Tutorial Bet"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};
declare
%rest:path("/GroupBaseX/TutorialHitStand")
%output:method("html")
%rest:GET
function BlackJack-controller:drawTutorialHitStand(){
  let $xslStylesheet := "Tutorial/GroupBaseXBlackJackTutorialHitStand.xslt"
  let $title := "GroupBaseX BlackJack Tutorial Hit Stand"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};
declare
%rest:path("/GroupBaseX/TutorialHitStandContinued")
%output:method("html")
%rest:GET
function BlackJack-controller:drawTutorialHitStandTutorialHitStandContinued(){
  let $xslStylesheet := "Tutorial/GroupBaseXBlackJackTutorialHitStandContinued.xslt"
  let $title := "GroupBaseX BlackJack Tutorial Hit Stand"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};
declare
%rest:path("/GroupBaseX/TutorialEnd")
%output:method("html")
%rest:GET
function BlackJack-controller:drawTutorialHitStandTutorialEnd(){
  let $xslStylesheet := "Tutorial/GroupBaseXBlackJackTutorialEnd.xslt"
  let $title := "GroupBaseX BlackJack Tutorial End"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare
%rest:path("/GroupBaseX/{$id}/choosePlayer")
%output:method("html")
%rest:GET
function BlackJack-controller:drawChoosePlayer($id){
  let $xslStylesheet := concat ("Lobby/GroupBaseXBlackJackLobbyChoosePlayer", $id, ".xslt")
  let $title := "GroupBaseX BlackJack Choose Player"
  let $game := BlackJack-main:getRootNode()
  return (BlackJack-controller:generatePage($game, $xslStylesheet, $title) )
};

declare function BlackJack-controller:generatePage($game as element(BlackJack), $xslStylesheet as xs:string, $title as xs:string){
    let $stylesheet := doc(concat($BlackJack-controller:staticPath, "/XSL/", $xslStylesheet))
    let $transformed := xslt:transform($game, $stylesheet)
    return
        <html>
            <head>
                <title>{$title}</title>
                <link rel="stylesheet" type="text/css" href="/static/GroupBaseXblackjack/CSS/GroupBaseXBlackJack.css"/>
            </head>
            <body>
                {$transformed}
            </body>
        </html>
};
