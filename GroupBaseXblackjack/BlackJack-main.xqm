xquery version "3.0";

module namespace BlackJack-main = "BlackJack/Main";
import module namespace BlackJack-helper = "BlackJack/Helper" at "BlackJack-helper.xqm";
import module namespace random = "http://basex.org/modules/random";
declare variable $BlackJack-main:game := db:open("GroupBaseX")/BlackJack;

declare
%updating
function BlackJack-main:setFirstPlayer($id){
  let $firstPlayer := BlackJack-helper:getFirstPlayer($id)
  return(
    replace value of node BlackJack-main:getGame($id)/PlayerTurn with $firstPlayer/@id
  )
};

declare
%updating
function BlackJack-main:updatePlayerNbr($id){
  let $playerNbr := count(BlackJack-main:getGame($id)/Characters/Player[@online = 1])
  return(
    replace value of node BlackJack-main:getGame($id)/@nbrPlayer with $playerNbr -1
  )
};

declare
%updating
function BlackJack-main:updateGamePhase($id){
  let $nextGamePhase := (BlackJack-main:getGame($id)/GamePhase + 1) mod 3
  return(
    replace value of node BlackJack-main:getGame($id)/GamePhase with $nextGamePhase
  )
};

declare
%updating
function BlackJack-main:updatePlayerTurn($id, $playerID){
  let $playerTurn := BlackJack-main:getGame($id)/PlayerTurn
  return(
    replace value of node $playerTurn with $playerID
  )
};

declare
%updating
function BlackJack-main:updateStock($id, $player, $profit){
  let $playerStock := $player/Stock
  return(
    replace value of node $playerStock with $playerStock + $profit
  )
};

declare
%updating
function BlackJack-main:processAddChip($id, $value){
  let $player := BlackJack-helper:getCurrentPlayer($id)
  return (
    replace value of node $player/CurrentBet with $player/CurrentBet + $value,
    replace value of node $player/Stock with $player/Stock - $value,
    insert node <Chip><Nbr>{$value}</Nbr></Chip> into $player/Chips
  )
};

declare
%updating
function BlackJack-main:processUndo($id){
  let $player := BlackJack-helper:getCurrentPlayer($id)
  let $Chips := $player/Chips
  return (
    replace value of node $player/Stock with $player/Stock + $player/Chips/Chip[last()],
    replace value of node $player/CurrentBet with $player/CurrentBet - $player/Chips/Chip[last()],
    delete node $player/Chips/Chip[last()]
  )
};

declare
%updating
function BlackJack-main:newDeck($id){
  let $newDeck := BlackJack-helper:newDeck()
  return(
    insert node $newDeck into BlackJack-main:getGame($id)
  )
};

declare 
%updating
function BlackJack-main:drawCard($id, $playerID){
  let $card := BlackJack-helper:randCard($id)
  let $value := $card/Value
  return (
    BlackJack-main:updateDeck($id, $playerID, $card),
    BlackJack-main:updateScore($id, $playerID, $value)
  )
};

declare 
%updating
function BlackJack-main:updateDeck($id, $playerID, $card){
  let $path := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]
  return 
    (insert node $card into $path/Cards,
    delete node $path/$card)
};

declare 
%updating
function BlackJack-main:updateScore($id, $playerID , $value as xs:double){
  let $playerScore := BlackJack-main:getGame($id)/Characters/Player[@id = $playerID]/Score
  let $newScore := $value + $playerScore
  return (
    replace value of node $playerScore with $newScore
  )
};

declare
%updating
function BlackJack-main:checkAss($id){
  let $playersOnline := BlackJack-main:getGame($id)/Characters/Player[@online = 1]
  return (
    for $player in $playersOnline return (
      if ($player/Score = 22) then (
        replace value of node $player/Cards/Card[Value = 11][1]/Value with 1,
        replace value of node $player/Score with 12
    )
  )
)
};

declare
%updating
function BlackJack-main:updateHighscore($id, $player){
  let $playerName := concat("", $player/@name)
  let $playerStock := concat("", $player/Stock)
  let $old := BlackJack-main:getRootNode()/HighScore/Player[@name = $playerName]
      return (
        if (count($old) = 0 and $player/number(Stock) != 0) then insert node <Player name="{$playerName}" score="{$playerStock}"/> into BlackJack-main:getRootNode()/HighScore
        else if ($old/number(@score) < $player/number(Stock)) then
          (insert node <Player name="{$playerName}" score="{$playerStock}"/> into BlackJack-main:getRootNode()/HighScore,
          delete node $old)
        )
};

declare
%updating
function BlackJack-main:updateHighscoreSort(){
      let $highscore := BlackJack-main:getRootNode()/HighScore
      let $sortedPlayers :=
        for $player in $highscore/Player
          order by $player/number(@score) descending
          return $player
  return (
    replace node $highscore with <HighScore>{$sortedPlayers}</HighScore>)
};

declare
%updating
function BlackJack-main:resetRound($id){
  let $playersOnline := BlackJack-main:getGame($id)/Characters/Player[@online = 1]
  return (
      for $player in $playersOnline return (
        if ($player/Stock = 0) then (replace value of node $player/@online with 0),
        replace value of node $player/Score with 0,
        replace value of node $player/CurrentBet with 0,
        delete node $player/Cards,
        delete node $player/Chips,
        insert node <Cards></Cards> into $player,
        insert node <Chips></Chips> into $player
      ),
      delete node BlackJack-main:getGame($id)/Deck
    )
};

declare function BlackJack-main:getGame($id){
  $BlackJack-main:game/Game[@id = $id]
};

declare function BlackJack-main:getRootNode(){
  $BlackJack-main:game
};