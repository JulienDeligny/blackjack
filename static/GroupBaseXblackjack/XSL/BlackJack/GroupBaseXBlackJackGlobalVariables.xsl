<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



    <xsl:variable name="BorderWidth" select="10"/>

    <!-- Orange Background Variables -->
    <xsl:variable name="OrangeBackgroundRectX" select="100"/>
    <xsl:variable name="OrangeBackgroundRectY" select="100"/>
    <xsl:variable name="OrangeBackgroundRectWidth" select="1800"/>
    <xsl:variable name="OrangeBackgroundRectHeight" select="400"/>
    <xsl:variable name="BackgroundTableEllCX" select="1000"/>
    <xsl:variable name="BackgroundEllCY" select="500"/>
    <xsl:variable name="OrangeBackgroundEllRX" select="900"/>
    <xsl:variable name="OrangeBackgroundEllRY" select="400"/>

    <!-- White Background Variables-->
    <xsl:variable name="WhiteBorderBBackgroundRectX" select="$OrangeBackgroundRectX + $BorderWidth"/>
    <xsl:variable name="WhiteBorderBBackgroundRectY" select="$OrangeBackgroundRectY + $BorderWidth"/>
    <xsl:variable name="WhiteBorderBBackgroundRectWidth" select="$OrangeBackgroundRectWidth - $BorderWidth *2"/>
    <xsl:variable name="WhiteBorderBBackgroundRectHeight" select="$OrangeBackgroundRectHeight - $BorderWidth"/>
    <xsl:variable name="WhiteBorderBBackgroundEllRX" select="$OrangeBackgroundEllRX - $BorderWidth"/>
    <xsl:variable name="WhiteBorderBBackgroundEllRY" select="$OrangeBackgroundEllRY - $BorderWidth"/>

    <!-- Green Background Variables-->
    <xsl:variable name="GreenBackgroundRectX" select="$OrangeBackgroundRectX + $BorderWidth *2"/>
    <xsl:variable name="GreenBackgroundRectY" select="$OrangeBackgroundRectY + $BorderWidth*2"/>
    <xsl:variable name="GreenBackgroundRectWidth" select="$OrangeBackgroundRectWidth - $BorderWidth *4"/>
    <xsl:variable name="GreenBackgroundRectHeight" select="$OrangeBackgroundRectHeight - $BorderWidth*2"/>
    <xsl:variable name="GreenBackgroundEllRX" select="$OrangeBackgroundEllRX - $BorderWidth*2"/>
    <xsl:variable name="GreenBackgroundEllRY" select="$OrangeBackgroundEllRY - $BorderWidth*2"/>

    <!-- HIT/STAND Button Variables -->
    <xsl:variable name="ButtonHitStandWhiteBackgroundCircleY" select="900"/>
    <xsl:variable name="ButtonHitCircleX" select="900"/>
    <xsl:variable name="ButtonStandCircleX" select="1100"/>
    <xsl:variable name="CircleHitStandRadWhiteBiggest" select="85"/>
    <xsl:variable name="CircleHitStandRadOrangeMiddle" select="75"/>
    <xsl:variable name="CircleHitStandRadWhiteLowest" select="65"/>

    <!-- Connection Boarder and Buttons Variables -->
    <xsl:variable name="ConnectionCircleX1" select="815"/>
    <xsl:variable name="ConnectionCircleX2" select="1170"/>
    <xsl:variable name="ConnectionCircleY" select="882"/>
    <xsl:variable name="ConnectionCircleWidth" select="15"/>
    <xsl:variable name="ConnectionCircleHeight" select="9.5"/>
    <xsl:variable name="BridgeX" select="970"/>
    <xsl:variable name="BridgeY" select="890"/>
    <xsl:variable name="BridgeWidth" select="60"/>
    <xsl:variable name="BridgeHeight" select="10"/>

    <!-- TEXT START Variables-->
    <xsl:variable name="TextLeftHitX" select="870"/>
    <xsl:variable name="TextStandHitY" select="910"/>
    <xsl:variable name="TextRightStandX" select="1038"/>
    <xsl:variable name="TextBlackJackMiddleX" select="850"/>
    <xsl:variable name="TextBlackJackMiddleY" select="365"/>
    <xsl:variable name="TextMiddleLeftX" select="590"/>
    <xsl:variable name="TextMiddleLeftY" select="200"/>
    <xsl:variable name="TextBlackJackSymbolsX" select="930"/>
    <xsl:variable name="TextBlackJackSymbolsY" select="425"/>
    <xsl:variable name="StandardFontSize" select="37"/>
    <xsl:variable name="IncreasedFontSize" select="55"/>
    <xsl:variable name="TextTurnOffset" select="570"/>

    <!-- Curved TEXT PATH >PAYS 2 to 1 - GRUPPE baseX<  START -->
    <xsl:variable name="CurvedTextPathStartX" select="600"/>
    <xsl:variable name="CurvedTextPathStartY" select="410"/>
    <xsl:variable name="CurvedTextPathMidX" select="400"/>
    <xsl:variable name="CurvedTextPathMidY" select="160"/>
    <xsl:variable name="CurvedTextPathEndX" select="800"/>
    <xsl:variable name="CurvedTextPathEndY" select="0"/>

    <!-- Dealer Rectangle Grey Variables-->
    <xsl:variable name="ValueDealerGreyRectangleX" select="850"/>
    <xsl:variable name="ValueDealerGreyRectangleY" select="150"/>
    <xsl:variable name="ValueDealerGreyRectangleWidth" select="300"/>
    <xsl:variable name="ValueDealerGreyRectangleHeight" select="175"/>
    <xsl:variable name="ValueDealerGreyRectangleRadius" select="20"/>

    <!-- Clickables Variables-->
    <xsl:variable name="ClickableWidth" select="200"/>
    <xsl:variable name="ClickableHeight" select="70"/>

    <!-- Score Rectangles Bright-Grey and Grey Variables-->
    <xsl:variable name="ScoreBackgroundX" select="100"/>
    <xsl:variable name="ScoreBackgroundY" select="10"/>
    <xsl:variable name="ScoreBackgroundWidth" select="1800"/>
    <xsl:variable name="ScoreBackgroundHeight" select="85"/>
    <xsl:variable name="DisplacementScoreRectangle" select="5"/>
    <xsl:variable name="ScoreBackgroundPlayerID1X" select="$ScoreBackgroundX + $DisplacementScoreRectangle"/>
    <xsl:variable name="ScoreBackgroundPlayerID2X" select="$ScoreBackgroundPlayerID1X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerID3X" select="$ScoreBackgroundPlayerID2X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerID4X" select="$ScoreBackgroundPlayerID3X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerID5X" select="$ScoreBackgroundPlayerID4X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerID6X" select="$ScoreBackgroundPlayerID5X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerID7X" select="$ScoreBackgroundPlayerID6X+$DisplacementScoreRectangle+$ScoreBackgroundPlayerWidth"/>
    <xsl:variable name="ScoreBackgroundPlayerY" select="15"/>
    <xsl:variable name="ScoreBackgroundPlayerWidth" select="251.4285714286"/>
    <xsl:variable name="ScoreBackgroundPlayerHeight" select="75"/>

    <!--Shifting of Cards Variables-->
    <xsl:variable name="OffsetCardX" select="20"/>
    <xsl:variable name="OffsetCardY" select="30"/>
    <xsl:variable name="OffsetPlayer" select="242"/>
    <xsl:variable name="OffsetChipX" select="10"/>
    <xsl:variable name="OffsetChipY" select="20"/>
    <xsl:variable name="MaxCardPerLine" select="5"/>

    <!--X Coordinates of the Cards Variables-->
    <xsl:variable name="PlayerCardsX" select="180"/>
    <!--Y Coordinates of the Cards(f.e. Player1 is on the same Height)-->
    <xsl:variable name="PlayerCards1Y" select="370"/>
    <xsl:variable name="PlayerCards2Y" select="510"/>
    <xsl:variable name="PlayerCards3Y" select="530"/>
    <xsl:variable name="CardWidthAndHeight" select="350"/>


    <!-- Chip Images and Background Variables-->
    <xsl:variable name="ButtonsRectangleX" select="10" />
    <xsl:variable name="ButtonsRectangleRadius" select="20" />
    <xsl:variable name="ButtonsRectangleWidth" select="725" />
    <xsl:variable name="ButtonsRectangleHeight" select="125" />
    <xsl:variable name="ButtonsRectangleStroke" select="5" />
    <xsl:variable name="ChipsHeightAndWidth" select="135" />
    <xsl:variable name="ButtonsRectangleY" select="890" />
    <xsl:variable name="ChipsButtonDistance" select="120" />

    <!-- All IN Buttons Variables-->
    <xsl:variable name="AllInRectangleY" select="730"/>
    <xsl:variable name="AllInTextX" select="55"/>
    <xsl:variable name="AllInTextY" select="775"/>

    <!-- UNDO Buttons Variables-->
    <xsl:variable name="UndoRechtangleX" select="220"/>
    <xsl:variable name="UndoRechtangleY" select="810"/>
    <xsl:variable name="UndoTextX" select="270"/>
    <xsl:variable name="UndoTextY" select="855"/>

    <!-- BET Buttons Variables-->
    <xsl:variable name="BetRechtangleY" select="810"/>
    <xsl:variable name="BetTextX" select="75"/>
    <xsl:variable name="BetTextY" select="855"/>

    <!-- ADD / DEL Buttons Variables-->
    <xsl:variable name="AddDelX" select="1650"/>
    <xsl:variable name="AddDelY" select="775"/>
    <xsl:variable name="AddDelTextX" select="1690"/>
    <xsl:variable name="AddDelTextY" select="825"/>

    <!-- NEW ROUND Buttons Variables-->
    <xsl:variable name="NewRoundX" select="1650"/>
    <xsl:variable name="NewRoundY" select="855"/>
    <xsl:variable name="NewRoundTextX" select="1670"/>
    <xsl:variable name="NewRoundTextY" select="905"/>

    <!-- EXIT Buttons Variables-->
    <xsl:variable name="ExitX" select="1650"/>
    <xsl:variable name="ExitY" select="935"/>
    <xsl:variable name="ExitTextX" select="1745"/>
    <xsl:variable name="ExitTextY" select="985"/>


    <!-- HELP SCREEN VARIABLES -->
    <!-- Orange Background -->
    <xsl:variable name="LOrangeBackgroundRectX" select="100"/>
    <xsl:variable name="LOrangeBackgroundRectY" select="100"/>
    <xsl:variable name="LOrangeBackgroundRectWidth" select="1800"/>
    <xsl:variable name="LOrangeBackgroundRectHeight" select="800"/>

    <!-- White Boarder Background -->
    <xsl:variable name="LWhiteBorderBackgroundRectX" select="110"/>
    <xsl:variable name="LWhiteBorderBackgroundRectY" select="110"/>
    <xsl:variable name="LWhiteBorderBackgroundRectWidth" select="1780"/>
    <xsl:variable name="LWhiteBorderBackgroundRectHeight" select="780"/>

    <!-- Green Background -->
    <xsl:variable name="LGreenBackgroundRectX" select="120"/>
    <xsl:variable name="LGreenBackgroundRectY" select="120"/>
    <xsl:variable name="LGreenBackgroundRectWidth" select="1760"/>
    <xsl:variable name="LGreenBackgroundRectHeight" select="760"/>


    <!-- Left InformationBox Orange Background-->
    <xsl:variable name="SelectMenu1BiggestBackgroundRectX" select="202"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundRectY" select="330"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundRectWidth" select="466"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundRectHeight" select="400"/>

    <xsl:variable name="SelectMenu1BiggestBackgroundEllCX" select="400"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundEllCY" select="400"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundEllRX" select="200"/>
    <xsl:variable name="SelectMenu1BiggestBackgroundEllRY" select="100"/>

    <!-- Left InformationBox White Background-->
    <xsl:variable name="SelectMenu1MiddleBackgroundRectX" select="212"/>
    <xsl:variable name="SelectMenu1MiddleBackgroundRectY" select="340"/>
    <xsl:variable name="SelectMenu1MiddleBackgroundRectWidth" select="446"/>
    <xsl:variable name="SelectMenu1MiddleBackgroundRectHeight" select="380"/>

    <!-- Middle InformationBox Orange Background-->
    <xsl:variable name="SelectMenu2BiggestBackgroundRectX" select="770"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundRectY" select="330"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundRectWidth" select="466"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundRectHeight" select="400"/>

    <xsl:variable name="SelectMenu2BiggestBackgroundEllCX" select="400"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundEllCY" select="400"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundEllRX" select="200"/>
    <xsl:variable name="SelectMenu2BiggestBackgroundEllRY" select="100"/>

    <!-- Middle InformationBox White Background-->
    <xsl:variable name="SelectMenu2MiddleBackgroundRectX" select="780"/>
    <xsl:variable name="SelectMenu2MiddleBackgroundRectY" select="340"/>
    <xsl:variable name="SelectMenu2MiddleBackgroundRectWidth" select="446"/>
    <xsl:variable name="SelectMenu2MiddleBackgroundRectHeight" select="380"/>

    <!-- Right InformationBox Orange Background-->
    <xsl:variable name="SelectMenu3BiggestBackgroundRectX" select="1336"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundRectY" select="330"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundRectWidth" select="466"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundRectHeight" select="400"/>

    <xsl:variable name="SelectMenu3BiggestBackgroundEllCX" select="400"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundEllCY" select="400"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundEllRX" select="200"/>
    <xsl:variable name="SelectMenu3BiggestBackgroundEllRY" select="100"/>

    <!-- Right InformationBox Orange Background-->
    <xsl:variable name="SelectMenu3MiddleBackgroundRectX" select="1346"/>
    <xsl:variable name="SelectMenu3MiddleBackgroundRectY" select="340"/>
    <xsl:variable name="SelectMenu3MiddleBackgroundRectWidth" select="446"/>
    <xsl:variable name="SelectMenu3MiddleBackgroundRectHeight" select="380"/>

    <!-- Buttons -->
    <xsl:variable name="ClickableX" select="10"/>
    <xsl:variable name="ContentFontSize" select="27"/>

    <!-- (Help) Curved Text PATH START -->
    <xsl:variable name="HCurvedTextPathStartX" select="600"/>
    <xsl:variable name="HCurvedTextPathStartY" select="310"/>
    <xsl:variable name="HCurvedTextPathMidX" select="400"/>
    <xsl:variable name="HCurvedTextPathMidY" select="-160"/>
    <xsl:variable name="HCurvedTextPathEndX" select="800"/>
    <xsl:variable name="HCurvedTextPathEndY" select="0"/>
    <xsl:variable name="BiggerFontSize" select="44"/>


    <!-- SCORE SCREEN VARIABLES-->
    <!-- Curved Text PATH START -->
    <xsl:variable name="HCurvedPathStartX" select="600"/>
    <xsl:variable name="HCurvedPathStartY" select="310"/>
    <xsl:variable name="HCurvedPathMidX" select="400"/>
    <xsl:variable name="HCurvedPathMidY" select="-160"/>
    <xsl:variable name="HCurvedPathEndX" select="800"/>
    <xsl:variable name="HCurvedPathEndY" select="0"/>


    <!-- LOBBY SCREEN VARIABLES-->
    <!-- Curved Text PATH START -->
    <xsl:variable name="LCurvedTextPathStartX" select="500"/>
    <xsl:variable name="LCurvedTextPathStartY" select="310"/>
    <xsl:variable name="LCurvedTextPathMidX" select="500"/>
    <xsl:variable name="LCurvedTextPathMidY" select="-160"/>
    <xsl:variable name="LCurvedTextPathEndX" select="1000"/>
    <xsl:variable name="LCurvedTextPathEndY" select="0"/>

    <!-- Curved Text PATH START -->
    <xsl:variable name="ChipsLeftX" select="130"/>
    <xsl:variable name="ChipsRightX" select="1750"/>






</xsl:stylesheet>