<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="GroupBaseXBlackJackGlobalVariables.xsl"/>

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- SVG BLACKJACK-TABLE START -->
            <!-- Orange Background  -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$OrangeBackgroundRectX}"
                  y="{$OrangeBackgroundRectY}" width="{$OrangeBackgroundRectWidth}"
                  height="{$OrangeBackgroundRectHeight}"/>
            <ellipse id="OrangeEllipseBiggestBackground" class="orangeBackgroundStyle" cx="{$BackgroundTableEllCX}"
                     cy="{$BackgroundEllCY}" rx="{$OrangeBackgroundEllRX}" ry="{$OrangeBackgroundEllRY}"/>
            <!-- White Background  -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$WhiteBorderBBackgroundRectX}"
                  y="{$WhiteBorderBBackgroundRectY}" width="{$WhiteBorderBBackgroundRectWidth}"
                  height="{$WhiteBorderBBackgroundRectHeight}"/>
            <ellipse id="WhiteEllipseMiddleBackground" class="whiteBorderBackgroundStyle" cx="{$BackgroundTableEllCX}"
                     cy="{$BackgroundEllCY}" rx="{$WhiteBorderBBackgroundEllRX}" ry="{$WhiteBorderBBackgroundEllRY}"/>
            <!-- Green Background  -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$GreenBackgroundRectX}"
                  y="{$GreenBackgroundRectY}" width="{$GreenBackgroundRectWidth}"
                  height="{$GreenBackgroundRectHeight}"/>
            <ellipse id="GreenEllipseMiddleBackground" class="greenBackgroundStyle" cx="{$BackgroundTableEllCX}"
                     cy="{$BackgroundEllCY}" rx="{$GreenBackgroundEllRX}" ry="{$GreenBackgroundEllRY}"/>

            <!-- Button HIT Background START -->
            <!-- White Half Circle Button HIT -->
            <circle id="WhiteHalfCircleBiggestBackground1" class="whiteBorderBackgroundStyleTwo" cx="{$ButtonHitCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}" r="{$CircleHitStandRadWhiteBiggest}"/>
            <!-- Orange Border around Button HIT -->
            <circle id="OrangeCircleBiggestBackground1" class="orangeBackgroundStyle" cx="{$ButtonHitCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                    r="{$CircleHitStandRadOrangeMiddle}"/>
            <!-- White Circle inside Button HIT -->
            <circle id="WhiteCircleBiggestBackground1" class="whiteBorderBackgroundStyle" cx="{$ButtonHitCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                    r="{$CircleHitStandRadWhiteLowest}"/>
            <!-- Button HIT Background END -->

            <!-- Button STAND Background START -->
            <!-- White Half Circle Button STAND -->
            <circle id="WhiteHalfCircleBiggestBackground2" class="whiteBorderBackgroundStyleTwo" cx="{$ButtonStandCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}" r="{$CircleHitStandRadWhiteBiggest}"/>
            <!-- Orange Border around Button STAND -->
            <circle id="OrangeCircleBiggestBackground2" class="orangeBackgroundStyle" cx="{$ButtonStandCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                    r="{$CircleHitStandRadOrangeMiddle}"/>
            <!-- White Circle inside Button STAND -->
            <circle id="WhiteCircleBiggestBackground2" class="whiteBorderBackgroundStyle" cx="{$ButtonStandCircleX}"
                    cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                    r="{$CircleHitStandRadWhiteLowest}"/>

            <!-- Connection Boarder and Buttons START -->
            <!-- Orange Connection between HIT and left Boarder-->
            <rect id="FixLeftHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="{$ConnectionCircleX1}"
                  y="{$ConnectionCircleY}" width="{$ConnectionCircleWidth}" height="{$ConnectionCircleHeight}"/>
            <!-- Orange Connection between STAND and right Boarder-->
            <rect id="FixRightHalfCircleBiggestBackground2" class="orangeBackgroundStyle" x="{$ConnectionCircleX2}"
                  y="{$ConnectionCircleY}" width="{$ConnectionCircleWidth}" height="{$ConnectionCircleHeight}"/>
            <!-- Orange Connection between Buttons -->
            <rect id="FixMiddleHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="{$BridgeX}"
                  y="{$BridgeY}" width="{$BridgeWidth}" height="{$BridgeHeight}"/>

            <!-- TEXT START -->
            <!-- TEXT IN BUTTONS LEFT HIT-->
            <text id="TextLeftButtonHIT" class="buttonsPlayer" x="{$TextLeftHitX}" y="{$TextStandHitY}">HIT
            </text>
            <!-- TEXT IN BUTTONS RIGHT STAND-->
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="{$TextRightStandX}" y="{$TextStandHitY}">STAND
            </text>
            <!-- TEXT "BLACKJACK" MIDDLE -->
            <xsl:variable name="scoreDealer" select="BlackJack/Game[@id = 2]/Characters/Player[@id = 8]/Score"/>"
            <text id="TextMiddleLeft" class="buttonsPlayer" x="{$TextMiddleLeftX}" y="{$TextMiddleLeftY}">♧ Score:
                <xsl:value-of select="$scoreDealer"/> ♧
            </text>
            <text id="TextBJMiddle" class="buttonsPlayerBold" x="{$TextBlackJackMiddleX}" y="{$TextBlackJackMiddleY}">♧BLACKJACK♧
            </text>
            <text id="TextSignsMiddle" class="buttonsPlayerBold" x="{$TextBlackJackSymbolsX}" y="{$TextBlackJackSymbolsY}"
                >♡♧♢♤
            </text>
            <path id="CurvedPath"
                  d="M {$CurvedTextPathStartX} {$CurvedTextPathStartY} q {$CurvedTextPathMidX} {$CurvedTextPathMidY} {$CurvedTextPathEndX} {$CurvedTextPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$StandardFontSize - 1}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">PAYS 2 TO 1 ♤ GRUPPE baseX ♤ PAYS 2 TO 1</textPath>
            </text>
            <!-- SVG BLACKJACK-BUTTONS/CIRCLES END -->

            <!-- Grey Rectangle (Dealer and Scoreboard)-->
            <rect id="ValueDealer" class="displayingRec" x="{$ValueDealerGreyRectangleX}"
                  y="{$ValueDealerGreyRectangleY}" rx="{$ValueDealerGreyRectangleRadius}"
                  ry="{$ValueDealerGreyRectangleRadius}"
                  width="{$ValueDealerGreyRectangleWidth}"
                  height="{$ValueDealerGreyRectangleHeight}"/>
            <rect id="GeneralScoreBackground" class="displayingRec" x="{$ScoreBackgroundX}" y="{$ScoreBackgroundY}"
                  width="{$ScoreBackgroundWidth}"
                  height="{$ScoreBackgroundHeight}"/>

            <!-- Generation of Player-Information -->
            <xsl:variable name="id_CurrentPlayer" select="BlackJack/Game[@id = 2]/PlayerTurn"/>
            <xsl:variable name="Phase" select="BlackJack/Game[@id = 2]/GamePhase"/>

            <xsl:for-each select="BlackJack/Game[@id = 2]/Characters/Player">

                <xsl:variable name="OnlinePlayer" select="@online"/>
                <xsl:variable name="IdPlayer" select="@id"/>
                <xsl:variable name="namePlayer" select="@name"/>
                <xsl:variable name="nbrCards" select="count(Cards/Card)"/>
                <xsl:variable name="scorePlayer" select="Score"/>


                <xsl:if test="$OnlinePlayer = 1">
                    <xsl:if test="$IdPlayer &lt; 8">
                        <xsl:variable name="PlayerInformationX"
                                      select="$ScoreBackgroundX + $DisplacementScoreRectangle + ($IdPlayer -1)*($DisplacementScoreRectangle+ $ScoreBackgroundPlayerWidth)"/>
                        <xsl:variable name="fillColor">
                            <xsl:choose>
                                <xsl:when test="$scorePlayer &gt; 21">
                                    <xsl:value-of select="'darksalmon'"/>
                                </xsl:when>
                                <xsl:when test="$scorePlayer = 21">
                                    <xsl:value-of select="'palegreen'"/>
                                </xsl:when>

                                <xsl:when test="($Phase = 2) and ($scoreDealer &gt; 21) and ($scorePlayer &lt; 22)">
                                    <xsl:value-of select="'palegreen'"/>
                                </xsl:when>
                                <xsl:when
                                        test="($Phase = 2) and ($scoreDealer &lt; 22) and ($scorePlayer &lt; 22) and ($scorePlayer &gt; $scoreDealer)">
                                    <xsl:value-of select="'palegreen'"/>
                                </xsl:when>
                                <xsl:when
                                        test="($Phase = 2) and ($scoreDealer = $scorePlayer) and (($scoreDealer &lt; 22))">
                                    <xsl:value-of select="'whitesmoke'"/>
                                </xsl:when>
                                <xsl:when test="($Phase = 2)">
                                    <xsl:value-of select="'darksalmon'"/>
                                </xsl:when>
                                <xsl:when test="$id_CurrentPlayer = $IdPlayer">
                                    <xsl:value-of select="'lightskyblue'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="'whitesmoke'"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>

                        <!-- Generation of Player-Information(Score) Rectangles-->
                        <rect id="ScoreBackgroundPlayerID{$IdPlayer}" class="displayingRecTwo" x="{$PlayerInformationX}"
                              y="{$ScoreBackgroundPlayerY}"
                              width="{$ScoreBackgroundPlayerWidth}"
                              height="{$ScoreBackgroundPlayerHeight}" fill="{$fillColor}"/>
                        <!-- Generation of Player-Information(Score) Rectangles Content-->
                        <text id="ScoreDisplayingNameID{$IdPlayer}" class="displayingRecBold" x="{$PlayerInformationX}"
                              y="{$ScoreBackgroundPlayerY +15}">Name:
                            <xsl:value-of select="$namePlayer"/>
                        </text>
                        <text id="ScoreDisplayingPlayerCurrentBetID{$IdPlayer}" class="displayingRecNotBold"
                              x="{$PlayerInformationX}"
                              y="{$ScoreBackgroundPlayerY +45}">Current Bet:
                            <xsl:value-of select="CurrentBet"/>
                        </text>
                        <text id="ScoreDisplayingPlayerScoreID{$IdPlayer}" class="displayingRecNotBold"
                              x="{$PlayerInformationX}"
                              y="{$ScoreBackgroundPlayerY +60}">Score:
                            <xsl:value-of select="$scorePlayer"/>
                        </text>
                        <text id="ScoreDisplayingPlayerNameID{$IdPlayer}" class="displayingRecNotBold"
                              x="{$PlayerInformationX}"
                              y="{$ScoreBackgroundPlayerY +30}">Stock:
                            <xsl:value-of select="Stock"/>
                        </text>
                    </xsl:if>

                    <!--Generate Cards-->
                    <xsl:for-each select="Cards/Card">
                        <xsl:variable name="pos" select="position()"/>
                        <xsl:variable name="Nbr" select="Nbr"/>
                        <xsl:variable name="Sign" select="Sign"/>
                        <xsl:variable name="Link"
                                      select="concat('/static/GroupBaseXblackjack/Cards/Card',$Nbr,$Sign,'.svg')"/>
                        <!--Check if there are too many cards in one line-->
                        <xsl:variable name="newLine">
                            <xsl:choose>
                                <xsl:when test="$pos &gt; (2 * $MaxCardPerLine)">
                                    <xsl:value-of select="2"/>
                                </xsl:when>
                                <xsl:when test="$pos &gt; $MaxCardPerLine">
                                    <xsl:value-of select="1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="0"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <!--Offset for a new Cards-Line-->
                        <xsl:variable name="newLineOffsetX">
                            <xsl:choose>
                                <xsl:when test="$IdPlayer &lt; 3">
                                    <xsl:value-of select="1"/>
                                </xsl:when>
                                <xsl:when test="$IdPlayer &gt; 5">
                                    <xsl:value-of select="-1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="0"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <!-- Card Position Dealer-->
                        <xsl:choose>
                            <xsl:when test="$IdPlayer=8">
                                <xsl:variable name="DealerCardX">
                                    <xsl:choose>
                                        <xsl:when test="$nbrCards &lt; 3">
                                            <xsl:value-of select="750 + 130*$pos"/>
                                        </xsl:when>
                                        <xsl:when test="$nbrCards &lt; 4">
                                            <xsl:value-of select="850 + 50*$pos"/>
                                        </xsl:when>
                                        <xsl:when test="$nbrCards &lt; 5">
                                            <xsl:value-of select="825 + 50*$pos"/>
                                        </xsl:when>
                                        <xsl:when test="$nbrCards &lt; 6">
                                            <xsl:value-of select="827 + 40*$pos"/>
                                        </xsl:when>
                                        <xsl:when test="$nbrCards &lt; 8">
                                            <xsl:value-of select="820 + 30*$pos"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="833 + 15*$pos"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:variable name="DealerCardY" select="70"/>
                                <image x="{$DealerCardX}" y="{$DealerCardY}" width="{$CardWidthAndHeight}"
                                       height="{$CardWidthAndHeight}" xlink:href="{$Link}"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- Calculate x coordinate of Card-->
                                <xsl:variable name="CardPosX"
                                              select="$PlayerCardsX + ($IdPlayer - 1) * $OffsetPlayer + (($pos - 1) mod $MaxCardPerLine)  * $OffsetCardX + $newLine * ($newLineOffsetX * $OffsetCardX)"/>
                                <!-- Calculate y coordinate of Card-->
                                <xsl:variable name="CardPosY">
                                    <xsl:choose>
                                        <xsl:when test="$IdPlayer = 1 or $IdPlayer = 7">
                                            <xsl:value-of select=" $PlayerCards1Y + $newLine * $OffsetCardY"/>
                                        </xsl:when>
                                        <xsl:when test="$IdPlayer = 2 or $IdPlayer = 6">
                                            <xsl:value-of select=" $PlayerCards2Y + $newLine * $OffsetCardY"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="$PlayerCards3Y +  $newLine * $OffsetCardY"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <svg>
                                    <image x="{$CardPosX}" y="{$CardPosY}" width="{$CardWidthAndHeight}"
                                           height="{$CardWidthAndHeight}" xlink:href="{$Link}"/>
                                </svg>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>

                    <!--CHIPS START-->
                    <xsl:for-each select="Chips/Chip">
                        <xsl:variable name="ChipNumber" select="Nbr"/>
                        <xsl:variable name="LinkChip"
                                      select="concat('/static/GroupBaseXblackjack/Chips/Chip_',$ChipNumber,'.svg')"/>
                        <xsl:variable name="PositionChip" select="position() mod 10"/>
                        <xsl:variable name="ChipPosX"
                                      select="$PlayerCardsX + ($PositionChip - 1) * $OffsetChipX + ($IdPlayer - 1) * $OffsetPlayer"/>
                        <xsl:variable name="ChipPosY">
                            <xsl:choose>
                                <xsl:when test="$IdPlayer = 1 or $IdPlayer = 7">
                                    <xsl:value-of select=" $PlayerCards1Y"/>
                                </xsl:when>
                                <xsl:when test="$IdPlayer = 2 or $IdPlayer = 6">
                                    <xsl:value-of select=" $PlayerCards2Y"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$PlayerCards3Y"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <svg>
                            <image x="{$ChipPosX}" y="{$ChipPosY}" width="80" height="80" xlink:href="{$LinkChip}"/>
                        </svg>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>

            <!-- Chip Images and Background -->
            <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$ButtonsRectangleX}" y="{$ButtonsRectangleY}"
                  rx="{$ButtonsRectangleRadius}"
                  width="{$ButtonsRectangleWidth}"
                  height="{$ButtonsRectangleHeight}"/>
            <image id="HIT_Chip_1" x="{$ButtonsRectangleX + $ChipsButtonDistance*0}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
            <image id="HIT_Chip_5" x="{$ButtonsRectangleX + $ChipsButtonDistance*1}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
            <image id="HIT_Chip_10" x="{$ButtonsRectangleX + $ChipsButtonDistance*2}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
            <image id="HIT_Chip_25" x="{$ButtonsRectangleX + $ChipsButtonDistance*3}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
            <image id="HIT_Chip_100" x="{$ButtonsRectangleX + $ChipsButtonDistance*4}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
            <image id="HIT_Chip_500" x="{$ButtonsRectangleX + $ChipsButtonDistance*5}" y="{$ButtonsRectangleY}"
                   width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>

            <!-- TEXT Right: Turn -->

            <xsl:variable name="CurrentPlayerID" select="BlackJack/Game[@id = 2]/PlayerTurn"/>
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="{$TextMiddleLeftX+$TextTurnOffset}"
                  y="{$TextMiddleLeftY}">♧ Turn:<xsl:value-of
                    select="BlackJack/Game[@id = 2]/Characters/Player[@id = $CurrentPlayerID]/@name"/> ♧
            </text>

            <!-- START of Phase0 = Bet-Phase -->
            <xsl:if test="$Phase = 0">
                <xsl:variable name="Player_ID" select="BlackJack/Game[@id = 2]/PlayerTurn"/>
                <xsl:variable name="Stock" select="BlackJack/Game[@id = 2]/Characters/Player[@id = $Player_ID]/Stock"/>
                <xsl:variable name="AllInLink" select="concat('/GroupBaseX/2/allin/', $Stock)"/>

                <xsl:if test="$Stock &gt;= 1">
                    <a xlink:href="/GroupBaseX/2/addChip/1">
                        <image id="HIT_Chip_1" x="{$ButtonsRectangleX + $ChipsButtonDistance*0}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
                    </a>
                </xsl:if>

                <xsl:if test="$Stock &gt;= 5">
                    <a xlink:href="/GroupBaseX/2/addChip/5">
                        <image id="HIT_Chip_5" x="{$ButtonsRectangleX + $ChipsButtonDistance*1}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
                    </a>
                </xsl:if>
                <xsl:if test="$Stock &gt;= 10">
                    <a xlink:href="/GroupBaseX/2/addChip/10">
                        <image id="HIT_Chip_10" x="{$ButtonsRectangleX + $ChipsButtonDistance*2}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
                    </a>
                </xsl:if>
                <xsl:if test="$Stock &gt;= 35">
                    <a xlink:href="/GroupBaseX/2/addChip/25">
                        <image id="HIT_Chip_25" x="{$ButtonsRectangleX + $ChipsButtonDistance*3}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
                    </a>
                </xsl:if>
                <xsl:if test="$Stock &gt;= 100">
                    <a xlink:href="/GroupBaseX/2/addChip/100">
                        <image id="HIT_Chip_100" x="{$ButtonsRectangleX + $ChipsButtonDistance*4}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
                    </a>
                </xsl:if>
                <xsl:if test="$Stock &gt;= 500">
                    <a xlink:href="/GroupBaseX/2/addChip/500">
                        <image id="HIT_Chip_500" x="{$ButtonsRectangleX + $ChipsButtonDistance*5}"
                               y="{$ButtonsRectangleY}" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                               xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>
                    </a>
                </xsl:if>
                <!-- ALL IN Button -->
                <a xlink:href="{$AllInLink}">
                    <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$ButtonsRectangleX}" y="{$AllInRectangleY}"
                          rx="{$ButtonsRectangleRadius}" width="{$ClickableWidth}" height="{$ClickableHeight}"/>
                    <text x="{$AllInTextX}" y="{$AllInTextY}" font-weight="bold" font-size="{$StandardFontSize}px"
                          font-family="sans-serif">
                        ALL IN
                    </text>
                </a>


                <!-- UNDO Button -->
                <xsl:if test="BlackJack/Game[@id = 2]/Characters/Player[@id = $Player_ID]/Chips != ''">
                    <a xlink:href="/GroupBaseX/2/undo">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$UndoRechtangleX}"
                              y="{$UndoRechtangleY}"
                              rx="{$ButtonsRectangleRadius}"
                              width="{$ClickableWidth}"
                              height="{$ClickableHeight}"/>
                        <text x="{$UndoTextX}" y="{$UndoTextY}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            UNDO
                        </text>
                    </a>

                    <!-- BET Button -->
                    <a xlink:href="/GroupBaseX/2/bet">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$ButtonsRectangleX}"
                              y="{$BetRechtangleY}"
                              rx="{$ButtonsRectangleRadius}"
                              width="{$ClickableWidth}"
                              height="{$ClickableHeight}"/>
                        <text x="{$BetTextX}" y="{$BetTextY}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            BET
                        </text>
                    </a>

                </xsl:if>
            </xsl:if>

            <!-- START of Phase1 = HIT/STAND-Phase -->
            <xsl:if test="$Phase = 1">
                <xsl:variable name="id" select="BlackJack/Game[@id = 2]/PlayerTurn"/>
                <xsl:variable name="HitLink" select="concat('/GroupBaseX/2/hit/',$id)"/>
                <xsl:variable name="StandLink" select="concat('/GroupBaseX/2/stand/',$id)"/>
                <a xlink:href="{$HitLink}">
                    <circle id="WhiteCircleBiggestBackground1" class="middleBackgroundStyle" cx="{$ButtonHitCircleX}"
                            cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                            r="{$CircleHitStandRadWhiteLowest}"/>
                </a>
                <a xlink:href="{$StandLink}">
                    <circle id="WhiteCircleBiggestBackground2" class="middleBackgroundStyle" cx="{$ButtonStandCircleX}"
                            cy="{$ButtonHitStandWhiteBackgroundCircleY}"
                            r="{$CircleHitStandRadWhiteLowest}"/>
                </a>
            </xsl:if>

            <!-- START of Phase2 = Play-Round is over and a new Round can be started -->
            <xsl:if test="$Phase = 2">
                <a xlink:href="/GroupBaseX/2/choosePlayer">
                    <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$AddDelX}" y="{$AddDelY}"
                          rx="{$ButtonsRectangleRadius}"
                          width="{$ClickableWidth+65}"
                          height="{$ClickableHeight}"/>
                    <text x="{$AddDelTextX}" y="{$AddDelTextY}" font-weight="bold" font-size="{$StandardFontSize}px"
                          font-family="sans-serif">
                        ADD / DEL
                    </text>
                </a>


                <xsl:variable name="GameId" select="2"/>
                <xsl:if test="BlackJack/Game[@id = $GameId]/Characters/Player[@id = 1 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 2 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 3 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 4 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 5 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 6 and @online = 1]/Stock > 0  or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 7 and @online = 1]/Stock > 0 ">
                    <a xlink:href="/GroupBaseX/2/reset">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$NewRoundX}" y="{$NewRoundY}"
                              rx="{$ButtonsRectangleRadius}"
                              width="{$ClickableWidth+65}"
                              height="{$ClickableHeight}"/>
                        <text x="{$NewRoundTextX}" y="{$NewRoundTextY}" font-weight="bold"
                              font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            NEW ROUND
                        </text>
                    </a>

                </xsl:if>
            </xsl:if>

            <!-- Exit Game Button-->
            <a xlink:href="/GroupBaseX/ChooseGame">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="{$ExitX}" y="{$ExitY}"
                      rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+65}"
                      height="{$ClickableHeight}"/>
                <text x="{$ExitTextX}" y="{$ExitTextY}" font-weight="bold" font-size="{$StandardFontSize}px"
                      font-family="sans-serif">
                    EXIT
                </text>
            </a>


        </svg>
    </xsl:template>
</xsl:stylesheet>