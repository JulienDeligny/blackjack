<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%"
             viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin">
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="100" y="100" width="1800"
                  height="400"></rect>
            <ellipse id="OrangeEllipseBiggestBackground" class="orangeBackgroundStyle" cx="1000" cy="500" rx="900"
                     ry="400"></ellipse>
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="110" y="110" width="1780"
                  height="390"></rect>
            <ellipse id="WhiteEllipseMiddleBackground" class="whiteBorderBackgroundStyle" cx="1000" cy="500" rx="890"
                     ry="390"></ellipse>
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="120" y="120" width="1760" height="380"
                  ></rect>
            <ellipse id="GreenEllipseMiddleBackground" class="greenBackgroundStyle" cx="1000" cy="500" rx="880" ry="380"
                     ></ellipse>
            <circle id="WhiteHalfCircleBiggestBackground1" class="whiteBorderBackgroundStyleTwo" cx="900" cy="900" r="85"
                   ></circle>
            <circle id="OrangeCircleBiggestBackground1" class="orangeBackgroundStyle" cx="900" cy="900" r="75"
                    ></circle>
            <circle id="WhiteCircleBiggestBackground1" class="whiteBorderBackgroundStyle" cx="900" cy="900" r="65"
                    ></circle>
            <circle id="WhiteHalfCircleBiggestBackground2" class="whiteBorderBackgroundStyleTwo" cx="1100" cy="900" r="85"
                    ></circle>
            <circle id="OrangeCircleBiggestBackground2" class="orangeBackgroundStyle" cx="1100" cy="900" r="75"
                    ></circle>
            <circle id="WhiteCircleBiggestBackground2" class="whiteBorderBackgroundStyle" cx="1100" cy="900" r="65"
                    ></circle>
            <rect id="FixLeftHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="815" y="882" width="15"
                  height="9.5"></rect>
            <rect id="FixRightHalfCircleBiggestBackground2" class="orangeBackgroundStyle" x="1170" y="882" width="15"
                  height="9.5" ></rect>
            <rect id="FixMiddleHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="970" y="890" width="60"
                  height="10"></rect>
            <text id="TextLeftButtonHIT" class="buttonsPlayer" x="870" y="910">HIT
            </text>
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="1038" y="910">STAND
            </text>
            <text id="TextMiddleLeft" class="buttonsPlayer" x="590" y="200" >♧
                Score:0♧
            </text>
            <text id="TextBJMiddle" class="buttonsPlayerBold" x="850" y="365" >♧BLACKJACK♧
            </text>
            <text id="TextSignsMiddle" class="buttonsPlayerBold" x="930" y="425">♡♧♢♤
            </text>
            <path id="CurvedPath" d="M 600 410 q 400 160 800 0" fill="none"></path>
            <text font-weight="bold" font-size="36px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">PAYS 2 TO 1 ♤ GRUPPE baseX ♤ PAYS 2 TO 1</textPath>
            </text>

            <rect id="RedTutorialField2" class="displayingRecRed" x="5" y="450" width="1250"
                  height="610"></rect>
            <text y="450" font-size="35px" font-family="sans-serif">
                <tspan class="text" x="10" dx="25" dy="40">Schritt 2:</tspan>
                <tspan class="text" x="10" dx="25" dy="60">Um den Einsatz zu bestätigen, muss man</tspan>
                <tspan class="text" x="10" dx="25" dy="33">auf "BET" klicken.</tspan>
                <tspan class="text" x="10" dx="25" dy="33">Nachdem alle Spieler ihre Einsätze</tspan>
                <tspan class="text" x="10" dx="25" dy="33">getätigt haben, werden die Karten ausgeteilt.</tspan>
                <tspan class="text" x="10" dx="25" dy="33">Mit "UNDO" können Chips zurück genommen werden.</tspan>
            </text>

            <a xlink:href="/GroupBaseX/TutorialHitStand">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1030" y="470" rx="20" width="215" height="70"
                      ></rect>
                <text x="1040" y="520" font-weight="bold" font-size="37px" font-family="sans-serif">CONTINUE</text>
            </a>

            <a xlink:href="/GroupBaseX/TutorialBet">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1030" y="550" rx="20" width="215" height="70"
                      ></rect>
                <text x="1080" y="600" font-weight="bold" font-size="37px" font-family="sans-serif">BACK</text>
            </a>
            <rect id="ValueDealer" class="displayingRec" x="850" y="150" rx="20" ry="20" width="300" height="175"
                  ></rect>
            <rect id="GeneralScoreBackground" class="displayingRec" x="100" y="10" width="1800" height="85"
                  ></rect>
            <rect id="ScoreBackgroundPlayerID1" class="displayingRecBlue" x="105" y="15" width="251.4285714286" height="75"
                  ></rect>
            <text id="ScoreDisplayingNameID1" class="displayingRecBold" x="105" y="30" >Name:Grace Hopper
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID1" class="displayingRecNotBold" x="105" y="45">Stock:895
            </text>
            <text id="ScoreDisplayingPlayerScoreID1" class="displayingRecNotBold" x="105" y="60">Current Bet:105
            </text>
            <text id="ScoreDisplayingPlayerNameID1" class="displayingRecNotBold" x="105" y="75">Score:0
            </text>
            <svg>
                <image x="180" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            </svg>
            <svg>
                <image x="190" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"></image>
            </svg>
            <svg>
                <image x="200" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"></image>
            </svg>
            <svg>
                <image x="210" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"></image>
            </svg>
            <svg>
                <image x="220" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            </svg>
            <svg>
                <image x="230" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            </svg>
            <svg>
                <image x="240" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"></image>
            </svg>
            <rect id="ScoreBackgroundPlayerID2" class="displayingRecWhite" x="361.4285714286" y="15" width="251.4285714286"
                  height="75"></rect>

            <rect id="ScoreBackgroundPlayerID2" class="displayingRecWhite" x="361.4285714286" y="15" width="251.4285714286"
                  height="75"></rect>
            <text id="ScoreDisplayingNameID2" class="displayingRecBold" x="361.4285714286" y="30">Name:Marlyn Meltzer
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID2" class="displayingRecNotBold" x="361.4285714286" y="45"
                  >Stock:1000
            </text>
            <text id="ScoreDisplayingPlayerScoreID2" class="displayingRecNotBold" x="361.4285714286" y="60"
                  >Current Bet:0
            </text>
            <text id="ScoreDisplayingPlayerNameID2" class="displayingRecNotBold" x="361.4285714286" y="75"
                  >Score:0
            </text>
            <rect id="ScoreBackgroundPlayerID6" class="displayingRecWhite" x="1387.142857143" y="15" width="251.4285714286"
                  height="75"></rect>
            <text id="ScoreDisplayingNameID6" class="displayingRecBold" x="1387.142857143" y="30">Name:Kathleen Antonelli
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID6" class="displayingRecNotBold" x="1387.142857143" y="45"
                  >Stock:1000
            </text>
            <text id="ScoreDisplayingPlayerScoreID6" class="displayingRecNotBold" x="1387.142857143" y="60"
                  >Current Bet:0
            </text>
            <text id="ScoreDisplayingPlayerNameID6" class="displayingRecNotBold" x="1387.142857143" y="75"
                  >Score:0
            </text>
            <rect id="ChipsGreyBackground" class="displayingRecButton" x="10" y="890" rx="20" width="725" height="125"
                  ></rect>
            <image id="HIT_Chip_1" x="10" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"></image>
            <image id="HIT_Chip_5" x="130" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"></image>
            <image id="HIT_Chip_10" x="250" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"></image>
            <image id="HIT_Chip_25" x="370" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            <image id="HIT_Chip_100" x="490" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"></image>
            <image id="HIT_Chip_500" x="610" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="1160" y="200">♧ Turn:Grace Hopper♧
            </text>
            <a>
                <image id="HIT_Chip_1" x="10" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"></image>
            </a>
            <a>
                <image id="HIT_Chip_5" x="130" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"></image>
            </a>
            <a>
                <image id="HIT_Chip_10" x="250" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"></image>
            </a>
            <a>
                <image id="HIT_Chip_25" x="370" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            </a>
            <a>
                <image id="HIT_Chip_100" x="490" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"></image>
            </a>
            <a>
                <image id="HIT_Chip_500" x="610" y="890" width="135" height="135"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </a>
            <a>
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="10" y="730" rx="20" width="200" height="70"
                      ></rect>
                <text x="55" y="775" font-weight="bold" font-size="37px" font-family="sans-serif">ALL IN</text>
            </a>
            <a>
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="10" y="810" rx="20" width="200" height="70"
                      ></rect>
                <text x="75" y="855" font-weight="bold" font-size="37px" font-family="sans-serif">BET</text>
            </a>
            <a>
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="220" y="810" rx="20" width="200" height="70"
                      ></rect>
                <text x="270" y="855" font-weight="bold" font-size="37px" font-family="sans-serif">UNDO</text>
            </a>
            <a xlink:href="/GroupBaseX/LobbyStart">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1700" y="935" rx="20" width="215" height="70"
                      ></rect>
                <text x="1765" y="985" font-weight="bold" font-size="37px" font-family="sans-serif">EXIT</text>
            </a>

        </svg>
    </xsl:template>
</xsl:stylesheet>
