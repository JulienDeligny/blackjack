<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%"
             viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin">
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="100" y="100" width="1800"
                  height="400"></rect>
            <ellipse id="OrangeEllipseBiggestBackground" class="orangeBackgroundStyle" cx="1000" cy="500" rx="900"
                     ry="400"></ellipse>
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="110" y="110" width="1780"
                  height="390"></rect>
            <ellipse id="WhiteEllipseMiddleBackground" class="whiteBorderBackgroundStyle" cx="1000" cy="500" rx="890"
                     ry="390"></ellipse>
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="120" y="120" width="1760" height="380"
                  ></rect>
            <ellipse id="GreenEllipseMiddleBackground" class="greenBackgroundStyle" cx="1000" cy="500" rx="880" ry="380"
                     ></ellipse>
            <circle id="WhiteHalfCircleBiggestBackground1" class="whiteBorderBackgroundStyleTwo" cx="900" cy="900" r="85"
                    ></circle>
            <circle id="OrangeCircleBiggestBackground1" class="orangeBackgroundStyle" cx="900" cy="900" r="75"
                   ></circle>
            <circle id="WhiteCircleBiggestBackground1" class="whiteBorderBackgroundStyle" cx="900" cy="900" r="65"
                    ></circle>
            <circle id="WhiteHalfCircleBiggestBackground2" class="whiteBorderBackgroundStyleTwo" cx="1100" cy="900" r="85"
                    ></circle>
            <circle id="OrangeCircleBiggestBackground2" class="orangeBackgroundStyle" cx="1100" cy="900" r="75"
                    ></circle>
            <circle id="WhiteCircleBiggestBackground2" class="whiteBorderBackgroundStyle" cx="1100" cy="900" r="65"
                    ></circle>
            <rect id="FixLeftHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="815" y="882" width="15"
                  height="9.5" ></rect>
            <rect id="FixRightHalfCircleBiggestBackground2" class="orangeBackgroundStyle" x="1170" y="882" width="15"
                  height="9.5" ></rect>
            <rect id="FixMiddleHalfCircleBiggestBackground1" class="orangeBackgroundStyle" x="970" y="890" width="60"
                  height="10" ></rect>
            <text id="TextLeftButtonHIT" class="buttonsPlayer" x="870" y="910">HIT
            </text>
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="1038" y="910">STAND</text>"
            <text id="TextMiddleLeft" class="buttonsPlayer" x="590" y="200">♧
                Score:17♧
            </text>
            <text id="TextBJMiddle" class="buttonsPlayerBold" x="850" y="365">♧BLACKJACK♧
            </text>
            <text id="TextSignsMiddle" class="buttonsPlayer" x="930" y="425">♡♧♢♤
            </text>
            <path id="CurvedPath" d="M 600 410 q 400 160 800 0" fill="none"></path>
            <text font-weight="bold" font-size="36px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">PAYS 2 TO 1 ♤ GRUPPE baseX ♤ PAYS 2 TO 1</textPath>
            </text>
            <rect id="RedTutorialField3.2" class="displayingRecRed" x="0" y="0" width="2000"
                  height="145"></rect>


            <rect id="ValueDealer" class="displayingRec" x="850" y="150" rx="20" ry="20" width="300" height="175"
                  ></rect>
            <rect id="GeneralScoreBackground" class="displayingRec" x="100" y="10" width="1800" height="85"
                  ></rect>
            <rect id="ScoreBackgroundPlayerID1" class="displayingRecDark" x="105" y="15" width="251.4285714286" height="75"
                  ></rect>
            <text id="ScoreDisplayingNameID1" class="displayingRecBold" x="105" y="30">Name:Grace Hopper
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID1" class="displayingRecNotBold" x="105" y="60">Current Bet:1000
            </text>
            <text id="ScoreDisplayingPlayerScoreID1" class="displayingRecNotBold" x="105" y="75">Score:22
            </text>
            <text id="ScoreDisplayingPlayerNameID1" class="displayingRecNotBold" x="105" y="45">Stock:0
            </text>
            <svg>
                <image x="180" y="370" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card8S.svg"></image>
            </svg>
            <svg>
                <image x="200" y="370" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card7C.svg"></image>
            </svg>
            <svg>
                <image x="220" y="370" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card7H.svg"></image>
            </svg>
            <svg>
                <image x="180" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <svg>
                <image x="190" y="370" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <rect id="ScoreBackgroundPlayerID2" class="displayingRecGreen" x="361.4285714286" y="15" width="251.4285714286"
                  height="75"></rect>
            <text id="ScoreDisplayingNameID2" class="displayingRecBold" x="361.4285714286" y="30">Name:Marlyn Meltzer
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID2" class="displayingRecNotBold" x="361.4285714286" y="60"
                  >Current Bet:1000
            </text>
            <text id="ScoreDisplayingPlayerScoreID2" class="displayingRecNotBold" x="361.4285714286" y="75">Score:21
            </text>
            <text id="ScoreDisplayingPlayerNameID2" class="displayingRecNotBold" x="361.4285714286" y="45"
                  >Stock:2000
            </text>
            <svg>
                <image x="422" y="510" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card7D.svg"></image>
            </svg>
            <svg>
                <image x="442" y="510" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card4D.svg"></image>
            </svg>
            <svg>
                <image x="462" y="510" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card10D.svg"></image>
            </svg>
            <svg>
                <image x="422" y="510" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <svg>
                <image x="432" y="510" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <rect id="ScoreBackgroundPlayerID6" class="displayingRecDark" x="1387.142857143" y="15" width="251.4285714286"
                  height="75"></rect>
            <text id="ScoreDisplayingNameID6" class="displayingRecBold" x="1387.142857143" y="30">Name:Kathleen Antonelli
            </text>
            <text id="ScoreDisplayingPlayerCurrentBetID6" class="displayingRecNotBold" x="1387.142857143" y="60"
                  >Current Bet:1000
            </text>
            <text id="ScoreDisplayingPlayerScoreID6" class="displayingRecNotBold" x="1387.142857143" y="75"
                  >Score:15
            </text>
            <text id="ScoreDisplayingPlayerNameID6" class="displayingRecNotBold" x="1387.142857143" y="45"
                  >Stock:0
            </text>
            <svg>
                <image x="1390" y="510" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/CardJC.svg"></image>
            </svg>
            <svg>
                <image x="1410" y="510" width="350" height="350"
                       xlink:href="/static/GroupBaseXblackjack/Cards/Card5S.svg"></image>
            </svg>
            <svg>
                <image x="1390" y="510" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <svg>
                <image x="1400" y="510" width="80" height="80"
                       xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            </svg>
            <image x="880" y="70" width="350" height="350"
                   xlink:href="/static/GroupBaseXblackjack/Cards/CardAH.svg"></image>
            <image x="1010" y="70" width="350" height="350"
                   xlink:href="/static/GroupBaseXblackjack/Cards/Card6H.svg"></image>
            <rect id="ChipsGreyBackground" class="DisplayingRec" x="10" y="890" rx="20" width="725" height="125"
                  fill="whitesmoke" stroke-width="5" stroke="darkgrey"></rect>
            <image id="HIT_Chip_1" x="10" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"></image>
            <image id="HIT_Chip_5" x="130" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"></image>
            <image id="HIT_Chip_10" x="250" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"></image>
            <image id="HIT_Chip_25" x="370" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"></image>
            <image id="HIT_Chip_100" x="490" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"></image>
            <image id="HIT_Chip_500" x="610" y="890" width="135" height="135"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"></image>
            <text id="TextRightButtonSTAND" class="buttonsPlayer" x="1160" y="200" >♧ Turn:Dealer♧
            </text>

            <rect id="RedTutorialField4" class="displayingRecRed" x="700" y="400" width="1250"
                  height="615" ></rect>


            <text y="400" font-size="35px" font-family="sans-serif">
                <tspan class="text" x="710" dx="25" dy="40">Schritt 5:</tspan>
                <tspan class="text" x="710" dx="25" dy="60">Mit 'KLICK' auf "ADD / DEL" kann man Spieler
                </tspan>
                <tspan class="text" x="710" dx="25" dy="33">am Ende einer Runde hinzufügen/entfernen.
                </tspan>
                <tspan class="text" x="710" dx="25" dy="33">"NEW ROUND" startet eine neue Runde mit  </tspan>
                <tspan class="text" x="710" dx="25" dy="33">den aktuellen Spielern. Aber Vorsicht! Es</tspan>
                <tspan class="text" x="710" dx="25" dy="33">muss immer mind. 1 Spieler am Tisch sitzen.</tspan>
                <tspan class="text" x="710" dx="25" dy="33">Mit 'KLICK' auf "EXIT" kommt man zurück zur Lobby.</tspan>
                <tspan class="text" x="710" dx="25" dy="60">Oben im Scoreboard erkennt man wer am Ende einer </tspan>
                <tspan class="text" x="710" dx="25" dy="33">Runde gewonnen oder verloren hat.</tspan>
                <tspan class="text" x="710" dx="25" dy="33">Grün steht hierbei für gewonnen. </tspan>
                <tspan class="text" x="710" dx="25" dy="33">Rot für verloren.</tspan>
                <tspan class="text" x="710" dx="25" dy="60">Sie haben das Tutorial nun durchschritten.</tspan>
                <tspan class="text" x="710" dx="25" dy="33">Mit "EXIT" verlassen sie das Tutorial. Viel Spaß!</tspan>

            </text>

            <a xlink:href="/GroupBaseX/TutorialHitStandContinued">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1670" y="420" rx="20" width="215" height="70"
                      ></rect>
                <text x="1720" y="470" font-weight="bold" font-size="37px" font-family="sans-serif">BACK</text>
            </a>




            <a>
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1650" y="775" rx="20" width="265" height="70"
                      ></rect>
                <text x="1690" y="825" font-weight="bold" font-size="37px" font-family="sans-serif">ADD / DEL</text>
            </a>
            <a>
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1650" y="855" rx="20" width="265" height="70"
                      ></rect>
                <text x="1670" y="905" font-weight="bold" font-size="37px" font-family="sans-serif">NEW ROUND</text>
            </a>
            <a xlink:href="/GroupBaseX/LobbyStart">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1650" y="935" rx="20" width="265" height="70"
                     ></rect>
                <text x="1745" y="985" font-weight="bold" font-size="37px" font-family="sans-serif">EXIT</text>
            </a>
        </svg>
    </xsl:template>
</xsl:stylesheet>
