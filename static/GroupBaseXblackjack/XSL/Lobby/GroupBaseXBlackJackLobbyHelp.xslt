<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="../BlackJack/GroupBaseXBlackJackGlobalVariables.xsl" />

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- Orange Background -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$LOrangeBackgroundRectX}"
                  y="{$LOrangeBackgroundRectY}" width="{$LOrangeBackgroundRectWidth}"
                  height="{$LOrangeBackgroundRectHeight}"/>

            <!-- White (Border) Background -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$LWhiteBorderBackgroundRectX}"
                  y="{$LWhiteBorderBackgroundRectY}" width="{$LWhiteBorderBackgroundRectWidth}"
                  height="{$LWhiteBorderBackgroundRectHeight}"/>

            <!-- Green Background -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$LGreenBackgroundRectX}"
                  y="{$LGreenBackgroundRectY}" width="{$LGreenBackgroundRectWidth}"
                  height="{$LGreenBackgroundRectHeight}"/>

            <!-- Left InformationBox -->
            <rect id="SelectMenu1OrangeRectBiggestBackground" class="biggestBackgroundStyle" x="{$SelectMenu1BiggestBackgroundRectX}"
                  y="{$SelectMenu1BiggestBackgroundRectY}" width="{$SelectMenu1BiggestBackgroundRectWidth}"
                  height="{$SelectMenu1BiggestBackgroundRectHeight}"/>

            <rect id="SelectMenu1WhiteRectMiddleBackground" class="middleBackgroundStyleTwo" x="{$SelectMenu1MiddleBackgroundRectX}"
                  y="{$SelectMenu1MiddleBackgroundRectY}" width="{$SelectMenu1MiddleBackgroundRectWidth}"
                  height="{$SelectMenu1MiddleBackgroundRectHeight}"/>
            <text  y="{$SelectMenu1MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif">
                    <tspan class="text" x="200" dx="25" dy="40">Developer:</tspan>
                    <tspan class="text" x="200" dx="25" dy="60">Julien Deligny</tspan>
                    <tspan class="text" x="200" dx="25" dy="30">Christina Reiter</tspan>
                    <tspan class="text" x="200" dx="25" dy="30">Isabella Rödl</tspan>
                    <tspan class="text" x="200" dx="25" dy="30">Christoph Weinhuber</tspan>
            </text>

            <!-- Middle InformationBox -->
            <rect id="SelectMenu2OrangeRectBiggestBackground" class="biggestBackgroundStyle" x="{$SelectMenu2BiggestBackgroundRectX}"
                  y="{$SelectMenu2BiggestBackgroundRectY}" width="{$SelectMenu2BiggestBackgroundRectWidth}"
                  height="{$SelectMenu2BiggestBackgroundRectHeight}"/>
            <rect id="SelectMenu2WhiteRectMiddleBackground" class="middleBackgroundStyleTwo" x="{$SelectMenu2MiddleBackgroundRectX}"
                  y="{$SelectMenu2MiddleBackgroundRectY}" width="{$SelectMenu2MiddleBackgroundRectWidth}"
                  height="{$SelectMenu2MiddleBackgroundRectHeight}"/>
            <text  y="{$SelectMenu2MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif">
                <tspan class="text" x="775" dx="25" dy="40">Tutorial:</tspan>
                <tspan class="text" x="775" dx="25" dy="60">"EXIT" → Startbildschirm </tspan>
                <tspan class="text" x="775" dx="25" dy="30">"SCORE" → Highscore Liste</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"HELP" → Hilfsmenü</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"PLAY" → Spiel starten</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"ALL IN" → Maximaler Einsatz</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"CHIPS" → Einsatz auswählen</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"BET" → Einsatz bestätigen</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"HIT" → Zusätzliche Karte</tspan>
                <tspan class="text" x="775" dx="25" dy="30">"STAND" → Keine weitere Karte</tspan>
            </text>


            <!-- Right InformationBox -->
            <rect id="SelectMenu3OrangeRectBiggestBackground" class="biggestBackgroundStyle" x="{$SelectMenu3BiggestBackgroundRectX}"
                  y="{$SelectMenu3BiggestBackgroundRectY}" width="{$SelectMenu3BiggestBackgroundRectWidth}"
                  height="{$SelectMenu3BiggestBackgroundRectHeight}"/>
            <rect id="SelectMenu3WhiteRectMiddleBackground" class="middleBackgroundStyleTwo" x="{$SelectMenu3MiddleBackgroundRectX}"
                  y="{$SelectMenu3MiddleBackgroundRectY}" width="{$SelectMenu3MiddleBackgroundRectWidth}"
                  height="{$SelectMenu3MiddleBackgroundRectHeight}"/>
            <text  y="{$SelectMenu3MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif">
                <tspan class="text" x="1350" dx="25" dy="40">Kurzform Regeln: </tspan>
                <tspan class="text" x="1350" dx="25" dy="60">Ziel ist es, die Augensumme des </tspan>
                <tspan class="text" x="1350" dx="25" dy="30">Dealers zu übertreffen ohne über</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">21 zu kommen. Jeder Spieler</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">bekommt 2 Karten, sieht jedoch</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">nur eine Karte des Dealers.</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">Der Spieler darf anschließend</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">wählen: weitere Karte oder nicht.</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">Karten 2-10 → 2-10 Ass → 1/11</tspan>
                <tspan class="text" x="1350" dx="25" dy="30">Bube, Dame, König → 10</tspan>
            </text>

            <!-- (Lobby) Curved Text PATH START -->
            <path id="CurvedPath" d="M {$HCurvedTextPathStartX} {$HCurvedTextPathStartY} q {$HCurvedTextPathMidX} {$HCurvedTextPathMidY} {$HCurvedTextPathEndX} {$HCurvedTextPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">♡ ♧ ♢ ♤ GRUPPE: baseX ♡ ♧ ♢ ♤</textPath>
            </text>

            <!-- Exit Lobby -->
            <a xlink:href="/GroupBaseX/LobbyStart">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1680" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1745" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    EXIT
                </text>
            </a>

            <!-- Score -->
            <a xlink:href="/GroupBaseX/LobbyScore">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="945" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    SCORE
                </text>
            </a>

            <!-- Help -->
            <a xlink:href="/GroupBaseX/LobbyHelp">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="100" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="155" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    HELP
                </text>
            </a>

            <!-- Auswahl2 Button -->
            <a xlink:href="/GroupBaseX/TutorialBet">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="685" rx="20"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="915" y="735" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    TUTORIAL
                </text>
            </a>


        </svg>
    </xsl:template>
</xsl:stylesheet>