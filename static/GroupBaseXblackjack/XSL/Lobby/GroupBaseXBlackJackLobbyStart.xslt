<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="../BlackJack/GroupBaseXBlackJackGlobalVariables.xsl" />

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- Orange Background -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$LOrangeBackgroundRectX}"
                  y="{$LOrangeBackgroundRectY}" width="{$LOrangeBackgroundRectWidth}"
                  height="{$LOrangeBackgroundRectHeight}"/>

            <!-- White (Border) Background -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$LWhiteBorderBackgroundRectX}"
                  y="{$LWhiteBorderBackgroundRectY}" width="{$LWhiteBorderBackgroundRectWidth}"
                  height="{$LWhiteBorderBackgroundRectHeight}"/>

            <!-- Green Background -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$LGreenBackgroundRectX}"
                  y="{$LGreenBackgroundRectY}" width="{$LGreenBackgroundRectWidth}"
                  height="{$LGreenBackgroundRectHeight}"/>

            <!-- Middle Graphic Cards and Rectangles -->
            <rect id="SelectMenu2OrangeRectBiggestBackground" class="biggestBackgroundStyle" x="{$SelectMenu2BiggestBackgroundRectX}"
                  y="{$SelectMenu2BiggestBackgroundRectY}" width="{$SelectMenu2BiggestBackgroundRectWidth}"
                  height="{$SelectMenu2BiggestBackgroundRectHeight}"/>
            <rect id="SelectMenu2WhiteRectMiddleBackground" class="middleBackgroundStyleTwo" x="{$SelectMenu2MiddleBackgroundRectX}"
                  y="{$SelectMenu2MiddleBackgroundRectY}" width="{$SelectMenu2MiddleBackgroundRectWidth}"
                  height="{$SelectMenu2MiddleBackgroundRectHeight}"/>
            <image x="1110" y="-170" width="600" height="600" transform="rotate(20)" xlink:href="/static/GroupBaseXblackjack/Cards/CardQC.svg"/>
            <image x="580" y="550" width="600" height="600" transform="rotate(-20)" xlink:href="/static/GroupBaseXblackjack/Cards/CardAC.svg"/>
            <image x="915" y="200" width="600" height="600"  xlink:href="/static/GroupBaseXblackjack/Cards/Card2S.svg"/>

            <!-- Curved Text PATH -->
            <path id="CurvedPath" d="M {$LCurvedTextPathStartX} {$LCurvedTextPathStartY} q {$LCurvedTextPathMidX} {$LCurvedTextPathMidY} {$LCurvedTextPathEndX} {$LCurvedTextPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">BLACKJACK ♤ GRUPPE baseX ♤ BLACKJACK</textPath>
            </text>
            <text  font-size="27px" font-family="sans-serif" x="560" y="870">
                Julien Deligny ♡ Christina Reiter ♧ Isabella Rödl ♢ Christoph Weinhuber
            </text>

            <!-- PLAY Button -->
            <a xlink:href="/GroupBaseX/ChooseGame">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="685" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="955" y="735" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    PLAY
                </text>
            </a>

            <!-- Chips -->
            <image id="HIT_Chip_1" x="{$ChipsLeftX}" y="150" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
            <image id="HIT_Chip_5" x="{$ChipsLeftX}" y="270" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
            <image id="HIT_Chip_10" x="{$ChipsLeftX}" y="390" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
            <image id="HIT_Chip_25" x="{$ChipsLeftX}" y="510" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
            <image id="HIT_Chip_100" x="{$ChipsLeftX}" y="630" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
            <image id="HIT_Chip_500" x="{$ChipsLeftX}" y="750" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>

            <image id="HIT_Chip_1" x="{$ChipsRightX}" y="150" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
            <image id="HIT_Chip_5" x="{$ChipsRightX}" y="270" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
            <image id="HIT_Chip_10" x="{$ChipsRightX}" y="390" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
            <image id="HIT_Chip_25" x="{$ChipsRightX}" y="510" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
            <image id="HIT_Chip_100" x="{$ChipsRightX}" y="630" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
            <image id="HIT_Chip_500" x="{$ChipsRightX}" y="750" width="{$ChipsHeightAndWidth}" height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>


            <!-- RESET Lobby Button -->
            <a xlink:href="/GroupBaseX/setup">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1680" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1730" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    RESET
                </text>
            </a>

            <!-- Score Button -->
            <a xlink:href="/GroupBaseX/LobbyScore">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="945" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    SCORE
                </text>
            </a>

            <!-- Help Button-->
            <a xlink:href="/GroupBaseX/LobbyHelp">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="100" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="155" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    HELP
                </text>
            </a>


        </svg>
    </xsl:template>
</xsl:stylesheet>