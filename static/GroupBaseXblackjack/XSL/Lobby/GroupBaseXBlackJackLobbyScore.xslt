<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="../BlackJack/GroupBaseXBlackJackGlobalVariables.xsl" />

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- Orange Background -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$LOrangeBackgroundRectX}"
                  y="{$LOrangeBackgroundRectY}" width="{$LOrangeBackgroundRectWidth}"
                  height="{$LOrangeBackgroundRectHeight}"/>

            <!-- White (Border) Background -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$LWhiteBorderBackgroundRectX}"
                  y="{$LWhiteBorderBackgroundRectY}" width="{$LWhiteBorderBackgroundRectWidth}"
                  height="{$LWhiteBorderBackgroundRectHeight}"/>

            <!-- Green Background -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$LGreenBackgroundRectX}"
                  y="{$LGreenBackgroundRectY}" width="{$LGreenBackgroundRectWidth}"
                  height="{$LGreenBackgroundRectHeight}" />

            <!-- Curved Text Path (Score) -->
            <path id="CurvedPath" d="M {$HCurvedPathStartX} {$HCurvedPathStartY} q {$HCurvedPathMidX} {$HCurvedPathMidY} {$HCurvedPathEndX} {$HCurvedPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">♡♧♢♤ TOP HIGHSCORES ♡♧♢♤</textPath>
            </text>

            <xsl:variable name="xCoordNameScore" select="820"/>
            <text text-anchor="end" y="{$SelectMenu2MiddleBackgroundRectY - 30}" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[1]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[2]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[3]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[4]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[5]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[6]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[7]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[8]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[9]/@name"/></tspan>
                <tspan class="text" x="{$xCoordNameScore}" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[10]/@name"/></tspan>
            </text>


            <text  y="{$SelectMenu2MiddleBackgroundRectY - 30}" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <xsl:if test="/BlackJack/HighScore/Player[1]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[2]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[3]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[4]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[5]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[6]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[7]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[8]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[9]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
                <xsl:if test="/BlackJack/HighScore/Player[10]/@score >= 0">
                <tspan class="text" x="850" dx="25" dy="50">......................................</tspan>
                </xsl:if>
            </text>

            <text  y="{$SelectMenu2MiddleBackgroundRectY - 30}" font-size="{$BiggerFontSize}px" font-family="sans-serif">

                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[1]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[2]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[3]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[4]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[5]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[6]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[7]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[8]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[9]/@score"/></tspan>
                <tspan class="text" x="1350" dx="25" dy="50"><xsl:value-of select="/BlackJack/HighScore/Player[10]/@score"/></tspan>
            </text>


            <!-- Exit Lobby Button -->
            <a xlink:href="/GroupBaseX/LobbyStart">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1680" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1745" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    EXIT
                </text>
            </a>

            <!-- Score Button -->
            <a xlink:href="/GroupBaseX/LobbyScore">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="945" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    SCORE
                </text>
            </a>

            <!-- Help Button-->
            <a xlink:href="/GroupBaseX/LobbyHelp">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="100" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="155" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    HELP
                </text>
            </a>

        </svg>
    </xsl:template>
</xsl:stylesheet>