<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="../BlackJack/GroupBaseXBlackJackGlobalVariables.xsl"/>

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- Orange Background -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$LOrangeBackgroundRectX}"
                  y="{$LOrangeBackgroundRectY}" width="{$LOrangeBackgroundRectWidth}"
                  height="{$LOrangeBackgroundRectHeight}"/>

            <!-- White (Border) Background -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$LWhiteBorderBackgroundRectX}"
                  y="{$LWhiteBorderBackgroundRectY}" width="{$LWhiteBorderBackgroundRectWidth}"
                  height="{$LWhiteBorderBackgroundRectHeight}"/>

            <!-- Green Background -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$LGreenBackgroundRectX}"
                  y="{$LGreenBackgroundRectY}" width="{$LGreenBackgroundRectWidth}"
                  height="{$LGreenBackgroundRectHeight}"/>


            <!-- Curved Text PATH -->
            <path id="CurvedPath"
                  d="M {$LCurvedTextPathStartX} {$LCurvedTextPathStartY} q {$LCurvedTextPathMidX} {$LCurvedTextPathMidY} {$LCurvedTextPathEndX} {$LCurvedTextPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">♡ ♧ ♢ ♤ CHOOSE PLAYER GAME 2 ♡ ♧ ♢ ♤</textPath>
            </text>


            <!-- Chips -->
            <image id="HIT_Chip_1" x="{$ChipsLeftX}" y="150" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
            <image id="HIT_Chip_5" x="{$ChipsLeftX}" y="270" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
            <image id="HIT_Chip_10" x="{$ChipsLeftX}" y="390" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
            <image id="HIT_Chip_25" x="{$ChipsLeftX}" y="510" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
            <image id="HIT_Chip_100" x="{$ChipsLeftX}" y="630" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
            <image id="HIT_Chip_500" x="{$ChipsLeftX}" y="750" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>

            <image id="HIT_Chip_1" x="{$ChipsRightX}" y="150" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_1.svg"/>
            <image id="HIT_Chip_5" x="{$ChipsRightX}" y="270" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_5.svg"/>
            <image id="HIT_Chip_10" x="{$ChipsRightX}" y="390" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_10.svg"/>
            <image id="HIT_Chip_25" x="{$ChipsRightX}" y="510" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_25.svg"/>
            <image id="HIT_Chip_100" x="{$ChipsRightX}" y="630" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_100.svg"/>
            <image id="HIT_Chip_500" x="{$ChipsRightX}" y="750" width="{$ChipsHeightAndWidth}"
                   height="{$ChipsHeightAndWidth}"
                   xlink:href="/static/GroupBaseXblackjack/Chips/Chip_500.svg"/>

            <xsl:variable name="NameOfPlayer1"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 1 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer2"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 2 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer3"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 3 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer4"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 4 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer5"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 5 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer6"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 6 and @online = 1]/@name"/>
            <xsl:variable name="NameOfPlayer7"
                          select="BlackJack/Game[@id = 2]/Characters/Player[@id = 7 and @online = 1]/@name"/>

            <foreignObject font-family="sans-serif" style="font-size:30px;" x="700" y="275" class="choosePlayer1"
                           width="100%" height="100%">
                <form class="choosePlayerGame1Form" action="/GroupBaseX/2/setPlayer">


                    <p>
                        <label>Player 1 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 1]/@online = 1">
                                <input type="text" name="PlayerName1" id="PlayerName1" value="{$NameOfPlayer1}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>

                            </xsl:otherwise>
                        </xsl:choose>


                    </p>


                    <p>
                        <label>Player 2 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 2]/@online = 1">
                                <input type="text" name="PlayerName2" id="PlayerName2" value="{$NameOfPlayer2}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>


                    <p>
                        <label>Player 3 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 3]/@online = 1">
                                <input type="text" name="PlayerName3" id="PlayerName3" value="{$NameOfPlayer3}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>


                    <p>
                        <label>Player 4 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 4]/@online = 1">
                                <input type="text" name="PlayerName4" id="PlayerName4" value="{$NameOfPlayer4}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>


                    <p>
                        <label>Player 5 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 5]/@online = 1">
                                <input type="text" name="PlayerName5" id="PlayerName5" value="{$NameOfPlayer5}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>


                    <p>
                        <label>Player 6 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 6]/@online = 1">
                                <input type="text" name="PlayerName6" id="PlayerName1" value="{$NameOfPlayer6}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>
                    <p>
                        <label>Player 7 :</label>
                        <xsl:choose>
                            <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 7]/@online = 1">
                                <input type="text" name="PlayerName7" id="PlayerName7" value="{$NameOfPlayer7}"
                                       style="font-size:30px;" font-family="sans-serif" maxlength="20"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <span style="padding-left:60px; color:seagreen; font-size: 40px">T</span>
                            </xsl:otherwise>
                        </xsl:choose>


                    </p>

                    <xsl:variable name="GameId" select="2"/>
                    <xsl:if test="BlackJack/Game[@id = $GameId]/Characters/Player[@id = 1]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 2]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 3]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 4]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 5]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 6]/@online = 1 or BlackJack/Game[@id = $GameId]/Characters/Player[@id = 7]/@online = 1">
                        <button style="font-size:30px;" class="submit" type="submit">START GAME</button>
                    </xsl:if>

                </form>
            </foreignObject>


            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 1]/@online = 1">

                    <!-- Remove Button 1 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/1">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 0}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 0}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 1 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/1">
                        <rect id="ChipsGreyBackground" class="DisplayingRecButton" x="890" y="{300 + 75 * 0}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 0}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>





            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 2]/@online = 1">

                    <!-- Remove Button 2 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/2">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 1}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 1}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 2 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/2">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 1}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 1}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>




            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 3]/@online = 1">

                    <!-- Remove Button 3 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/3">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 2}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 2}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 3 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/3">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 2}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 2}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>


            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 4]/@online = 1">

                    <!-- Remove Button 4 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/4">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 3}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 3}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 4 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/4">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 3}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 3}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>



            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 5]/@online = 1">

                    <!-- Remove Button 5 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/5">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 4}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 4}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 5 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/5">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 4}" rx="10"
                              width="280"
                              height="40" />
                        <text x="910" y="{333 + 75 * 4}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 6]/@online = 1">

                    <!-- Remove Button 6 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/6">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 5}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 5}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 6 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/6">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 5}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 5}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
                <xsl:when test="BlackJack/Game[@id = 2]/Characters/Player[@id = 7]/@online = 1">

                    <!-- Remove Button 7 -->
                    <a xlink:href="/GroupBaseX/2/removePlayer/7">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="1250" y="{300 + 75 * 6}" rx="10"
                              width="65"
                              height="40"/>
                        <text x="1270" y="{333 + 75 * 6}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            X
                        </text>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Add Button 7 -->
                    <a xlink:href="/GroupBaseX/2/addPlayer/7">
                        <rect id="ChipsGreyBackground" class="displayingRecButton" x="890" y="{300 + 75 * 6}" rx="10"
                              width="280"
                              height="40"/>
                        <text x="910" y="{333 + 75 * 6}" font-weight="bold" font-size="{$StandardFontSize}px"
                              font-family="sans-serif">
                            ADD PLAYER
                        </text>
                    </a>
                </xsl:otherwise>
            </xsl:choose>



            <!-- Exit Lobby -->
            <a xlink:href="/GroupBaseX/ChooseGame">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1680" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1745" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    EXIT
                </text>
            </a>


            <!-- Score Button -->
            <a xlink:href="/GroupBaseX/LobbyScore">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="945" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    SCORE
                </text>
            </a>

            <!-- Help Button-->
            <a xlink:href="/GroupBaseX/LobbyHelp">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="100" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="155" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    HELP
                </text>
            </a>


        </svg>
    </xsl:template>
</xsl:stylesheet>