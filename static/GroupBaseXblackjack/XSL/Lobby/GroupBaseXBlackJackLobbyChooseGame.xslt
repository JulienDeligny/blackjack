<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsk="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="../BlackJack/GroupBaseXBlackJackGlobalVariables.xsl"/>

    <xsl:template match="/">
        <svg width="100%" height="100%" viewBox="0 0 2000 1000" preserveAspectRatio="xMidYMin"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <!-- Orange Background -->
            <rect id="OrangeRectBiggestBackground" class="orangeBackgroundStyle" x="{$LOrangeBackgroundRectX}"
                  y="{$LOrangeBackgroundRectY}" width="{$LOrangeBackgroundRectWidth}"
                  height="{$LOrangeBackgroundRectHeight}"/>

            <!-- White (Border) Background -->
            <rect id="WhiteRectMiddleBackground" class="whiteBorderBackgroundStyle" x="{$LWhiteBorderBackgroundRectX}"
                  y="{$LWhiteBorderBackgroundRectY}" width="{$LWhiteBorderBackgroundRectWidth}"
                  height="{$LWhiteBorderBackgroundRectHeight}"/>

            <!-- Green Background -->
            <rect id="GreenRectMiddleBackground" class="greenBackgroundStyle" x="{$LGreenBackgroundRectX}"
                  y="{$LGreenBackgroundRectY}" width="{$LGreenBackgroundRectWidth}"
                  height="{$LGreenBackgroundRectHeight}"/>

            <!-- Left InformationBox -->
            <rect id="SelectMenu1OrangeRectBiggestBackground" class="biggestBackgroundStyle"
                  x="{$SelectMenu1BiggestBackgroundRectX}"
                  y="{$SelectMenu1BiggestBackgroundRectY}" width="{$SelectMenu1BiggestBackgroundRectWidth}"
                  height="{$SelectMenu1BiggestBackgroundRectHeight}"/>

            <rect id="SelectMenu1WhiteRectMiddleBackground" class="middleBackgroundStyleTwo"
                  x="{$SelectMenu1MiddleBackgroundRectX}"
                  y="{$SelectMenu1MiddleBackgroundRectY}" width="{$SelectMenu1MiddleBackgroundRectWidth}"
                  height="{$SelectMenu1MiddleBackgroundRectHeight}"/>

            <text y="{$SelectMenu3MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif"
                  font-weight="bold">
                <tspan class="text" x="200" dx="25" dy="40">Player online:
                    <xsl:value-of select="BlackJack/Game[@id = 1]/@nbrPlayer "/> / 7
                </tspan>
            </text>

            <image x="610" y="100" width="500" height="500" transform="rotate(20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/CardAD.svg"/>
            <image x="50" y="420" width="500" height="500" transform="rotate(-20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/CardAH.svg"/>
            <image x="350" y="270" width="500" height="500" xlink:href="/static/GroupBaseXblackjack/Cards/CardAS.svg"/>

            <!-- Middle InformationBox -->
            <rect id="SelectMenu2OrangeRectBiggestBackground" class="biggestBackgroundStyle"
                  x="{$SelectMenu2BiggestBackgroundRectX}"
                  y="{$SelectMenu2BiggestBackgroundRectY}" width="{$SelectMenu2BiggestBackgroundRectWidth}"
                  height="{$SelectMenu2BiggestBackgroundRectHeight}"/>
            <rect id="SelectMenu2WhiteRectMiddleBackground" class="middleBackgroundStyleTwo"
                  x="{$SelectMenu2MiddleBackgroundRectX}"
                  y="{$SelectMenu2MiddleBackgroundRectY}" width="{$SelectMenu2MiddleBackgroundRectWidth}"
                  height="{$SelectMenu2MiddleBackgroundRectHeight}"/>
            <text y="{$SelectMenu2MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif"
                  font-weight="bold">
                <tspan class="text" x="775" dx="25" dy="40">Player online:
                    <xsl:value-of select="BlackJack/Game[@id = 2]/@nbrPlayer "/> / 7
                </tspan>
            </text>

            <image x="1140" y="-100" width="500" height="500" transform="rotate(20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/Card2D.svg"/>
            <image x="580" y="620" width="500" height="500" transform="rotate(-20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/Card2H.svg"/>
            <image x="915" y="270" width="500" height="500" xlink:href="/static/GroupBaseXblackjack/Cards/Card2S.svg"/>

            <!-- Right InformationBox -->
            <rect id="SelectMenu3OrangeRectBiggestBackground" class="biggestBackgroundStyle"
                  x="{$SelectMenu3BiggestBackgroundRectX}"
                  y="{$SelectMenu3BiggestBackgroundRectY}" width="{$SelectMenu3BiggestBackgroundRectWidth}"
                  height="{$SelectMenu3BiggestBackgroundRectHeight}"/>
            <rect id="SelectMenu3WhiteRectMiddleBackground" class="middleBackgroundStyleTwo"
                  x="{$SelectMenu3MiddleBackgroundRectX}"
                  y="{$SelectMenu3MiddleBackgroundRectY}" width="{$SelectMenu3MiddleBackgroundRectWidth}"
                  height="{$SelectMenu3MiddleBackgroundRectHeight}"/>

            <text y="{$SelectMenu3MiddleBackgroundRectY}" font-size="{$ContentFontSize}px" font-family="sans-serif"
                  font-weight="bold">
                <tspan class="text" x="1350" dx="25" dy="40">Player online:
                    <xsl:value-of select="BlackJack/Game[@id = 3]/@nbrPlayer "/> / 7
                </tspan>
            </text>
            <image x="1690" y="-300" width="500" height="500" transform="rotate(20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/Card3D.svg"/>
            <image x="1140" y="810" width="500" height="500" transform="rotate(-20)"
                   xlink:href="/static/GroupBaseXblackjack/Cards/Card3H.svg"/>
            <image x="1500" y="270" width="500" height="500" xlink:href="/static/GroupBaseXblackjack/Cards/Card3S.svg"/>

            <!-- (Lobby) Curved Text PATH START -->
            <path id="CurvedPath"
                  d="M {$HCurvedTextPathStartX} {$HCurvedTextPathStartY} q {$HCurvedTextPathMidX} {$HCurvedTextPathMidY} {$HCurvedTextPathEndX} {$HCurvedTextPathEndY}"
                  fill="none"/>
            <text font-weight="bold" font-size="{$BiggerFontSize}px" font-family="sans-serif">
                <textPath xlink:href="#CurvedPath">♡ ♧ ♢ ♤ CHOOSE GAME ♡ ♧ ♢ ♤</textPath>
            </text>

            <!-- Exit Lobby -->
            <a xlink:href="/GroupBaseX/LobbyStart">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1680" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1745" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    EXIT
                </text>
            </a>

            <!-- Score -->
            <a xlink:href="/GroupBaseX/LobbyScore">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="945" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    SCORE
                </text>
            </a>

            <!-- Help -->
            <a xlink:href="/GroupBaseX/LobbyHelp">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="100" y="935" rx="{$ButtonsRectangleRadius}"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="155" y="985" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    HELP
                </text>
            </a>


            <xsl:variable name="gameLink1">
                <xsl:choose>
                    <xsl:when test="BlackJack/Game[@id = 1]/@nbrPlayer > 0">
                        <xsl:value-of select="'/GroupBaseX/1/draw'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'/GroupBaseX/1/choosePlayer'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>


            <!-- Auswahl1 Button -->
            <a xlink:href="{$gameLink1}">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="320" y="685" rx="20"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="355" y="735" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    GAME 1
                </text>
            </a>

            <xsl:variable name="gameLink2">
                <xsl:choose>
                    <xsl:when test="BlackJack/Game[@id = 2]/@nbrPlayer > 0">
                        <xsl:value-of select="'/GroupBaseX/2/draw'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'/GroupBaseX/2/choosePlayer'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- Auswahl2 Button -->
            <a xlink:href="{$gameLink2}">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="900" y="685" rx="20"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="935" y="735" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    GAME 2
                </text>
            </a>

            <xsl:variable name="gameLink3">
                <xsl:choose>
                    <xsl:when test="BlackJack/Game[@id = 3]/@nbrPlayer > 0">
                        <xsl:value-of select="'/GroupBaseX/3/draw'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'/GroupBaseX/3/choosePlayer'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>


            <!-- Auswahl3 Button -->
            <a xlink:href="{$gameLink3}">
                <rect id="ChipsGreyBackground" class="displayingRecButton" x="1470" y="685" rx="20"
                      width="{$ClickableWidth+15}"
                      height="{$ClickableHeight}"/>
                <text x="1505" y="735" font-weight="bold" font-size="{$StandardFontSize}px" font-family="sans-serif">
                    GAME 3
                </text>
            </a>


        </svg>
    </xsl:template>
</xsl:stylesheet>